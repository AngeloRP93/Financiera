export interface Usuario {
    usuario_id: number;
    nombres: string;
    email:string;
    apellido_materno:string;
    apellido_paterno:string;
    rol_id: number;
    rol_nombre:string;
    estado:boolean;
}