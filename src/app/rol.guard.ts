import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, UrlSegment } from '@angular/router';
import { Observable } from 'rxjs';
import { Usuario } from './usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class RolGuard implements CanActivate {
  private usuario: Usuario;
  constructor(private router: Router) {
  }


  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (localStorage.getItem('usuario') !== undefined) {
      this.usuario = JSON.parse(localStorage.getItem('usuario'));
    }

    if (this.usuario === undefined || this.usuario === null) {
      return this.haciaLogin();
    }
    const rol = this.usuario.rol_id + '';
    if (rol !== null && rol !== undefined) {
      return this.redireccionamiento(route.url[0]);
    } else {
      return this.haciaLogin();
    }
  }

  private haciaLogin(): boolean {
    this.router.navigate(['/auth']);
    return false;
  }

  private redireccionamiento(ruta_actual:UrlSegment): boolean {
    let ruta = this.usuario.rol_nombre.toLowerCase();
    if ((ruta_actual + '') == ruta) {
      return true;  
    } else {
      this.router.navigate(['/' + ruta],{replaceUrl:true});
      return false;
    }
    
  }
}
