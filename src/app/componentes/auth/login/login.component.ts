import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  login: FormGroup;
  hide = true;
  constructor(
    private _formBuilder: FormBuilder,
    private api:ApiService
  ) {
    localStorage.clear();
    this.inicializarFormulario();
  }

  ngOnInit() {
  }

  private inicializarFormulario() {
    this.login = this._formBuilder.group({
      'password': ['', Validators.required],
      'email': ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]+([.][a-zA-Z0-9]+)*@[a-zA-Z0-9]+([.][a-zA-Z0-9]+)*[.][a-zA-Z0-9]{1,40}')]],
    });
  }

  onSubmit() {
    if (this.login.valid) {
      this.api.login(this.login.value)
    }
  }

  emailValido(): boolean {
    return this.login.get('email').valid;
  }

  emailDirty(): boolean {
    return this.login.get('email').dirty;
  }

  contrasenaValida(): boolean {
    return this.login.get('password').valid;
  }

  getErrorMessage(): string {
    return 'Este campo es requerido';
  }

}
