import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LayoutVacioComponent } from './layout/layout.component';
import { MatButtonModule, MatIconModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ApiService } from '../../services/api.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AuthRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  declarations: [LayoutVacioComponent, LoginComponent],
  exports: [LoginComponent],
  providers: [
    ApiService
  ]
})
export class AuthModule { }
