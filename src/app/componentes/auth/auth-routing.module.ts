import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LayoutVacioComponent } from './layout/layout.component';

const routes: Routes = [
  {
      path: '',
      component: LayoutVacioComponent,
      children: [
          {
              path: '',
              component: LoginComponent
          }
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
