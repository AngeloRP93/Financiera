import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrincipalRoutingModule } from './principal-routing.module';
import { PieGraficosModule } from '../../../componentes-compartidos/pie-graficos/pie-graficos.module';
import { PrincipalComponent } from './principal/principal.component';
import { InfoModule } from '../../../componentes-compartidos/info/info.module';
import { TarjetasServicesModule } from '../../../services/tarjetas/tarjetas.module';
import { LocalesModule } from '../../../services/locales/locales.module';
import { FechasModule } from '../../../componentes-compartidos/fechas/fechas.module';
import { CrediventasServicesModule } from '../../../services/crediventas-services/crediventas-services.module';
import { OeschleServicesModule } from 'src/app/services/oeschle-services/oeschle-services.module';
import { SelectsModule } from '../../../componentes-compartidos/forms/form-content/selects/selects.module';

@NgModule({
  imports: [
    CommonModule,
    PrincipalRoutingModule,
    PieGraficosModule,
    InfoModule,
    SelectsModule,
    FechasModule,
    LocalesModule,
    TarjetasServicesModule,
    CrediventasServicesModule,
    OeschleServicesModule
  ],
  declarations: [PrincipalComponent]
})
export class PrincipalModule { }
