import { Component, OnInit } from '@angular/core';
import { LocalService } from '../../../../services/locales/local.service';
import { TarjetasService } from '../../../../services/tarjetas/tarjetas.service';
import * as moment from 'moment';
import { Router, ActivatedRoute } from '@angular/router';
import { CrediventasService } from '../../../../services/crediventas-services/crediventas.service';
import { OeschleService } from '../../../../services/oeschle-services/oeschle.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent implements OnInit {
  loading = true;
  loadingGraficos = true;
  locales: any;
  fechaInicioContenido: any;
  fechaFinContenido: any;
  porcentajeEntregadas = 0;
  porcentajeCrediventas = 0;
  noPorcentajeCrediventas = 0;
  mostrar_crediventas = false;
  porcentaje_oeschle_superior = 0;
  porcentaje_oeschle_inferior = 0;
  mostrar_oeschle = false;
  mensaje_oec = 'Sin data para esta tienda en el rango de fecha seleccionado'
  mensaje_vea = 'Sin data para esta tienda en el rango de fecha seleccionado'
  public hoy: any = new Date();
  private fin = moment().subtract(1, 'days').format('YYYY-MM-DD').split('-');
  public fechaFin: any = new Date(parseInt(this.fin[0]), parseInt(this.fin[1]) - 1, parseInt(this.fin[2]));
  public fechaInicio: any = new Date(this.hoy.getFullYear(), this.hoy.getMonth(), 1);
  titulo = 'Tarjetas';
  tituloInfo = 'VEA Salaverry';
  localKey = null;
  constructor(
    private localService: LocalService,
    private tarjetas_service: TarjetasService,
    private crediventas_service: CrediventasService,
    private oeschle_service: OeschleService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.fillFechaInicioContenido();
    this.fillFechaFinContenido();
  }

  haciaTarjetas() {
    this.router.navigate(['tarjetas'],{ relativeTo: this.route });
  }

  haciaCrediventas() {
    this.router.navigate(['crediventas'],{ relativeTo: this.route });
  }

  haciaOeschle() {
    this.router.navigate(['oeschle'],{ relativeTo: this.route });
  }

  private fillFechaInicioContenido() {
    let fecha_inicio = localStorage.getItem('fecha_inicio');
    if (
      fecha_inicio !== null &&
      fecha_inicio !== undefined
    ) {
      const transformado = this.darFormatoAnioMesDia(fecha_inicio)
      if (transformado !== null) {
        this.fechaInicio = new Date(transformado.anio, transformado.mes - 1, transformado.dia)
      }
    }
    this.fechaInicioContenido = {
      advertencia: {
        required: 'Fecha Requerida',
        min: '',
        max: '',
        pattern: 'Fecha inválida'
      },
      data: {
        value: this.fechaInicio,
        isRequired: true,
        id: 'fechaInicio',
        isSeteable: false
      }
    }
    localStorage.setItem('fecha_inicio', this.darFormatoFecha(this.fechaInicio));
  }

  private fillFechaFinContenido() {
    let fecha_fin = localStorage.getItem('fecha_fin');
    if (
      fecha_fin !== null &&
      fecha_fin !== undefined
    ) {
      const transformado = this.darFormatoAnioMesDia(fecha_fin)
      if (transformado !== null) {
        this.fechaFin = new Date(transformado.anio, transformado.mes - 1, transformado.dia)
      }
    }
    this.fechaFinContenido = {
      advertencia: {
        required: 'Fecha Requerida',
        min: 'Fecha invalida',
        max: 'Fecha invalida',
        pattern: 'Fecha inválida'
      },
      data: {
        value: this.fechaFin,
        isRequired: true,
        id: 'fechaFin',
        isSeteable: false
      }
    }
    localStorage.setItem('fecha_fin', this.darFormatoFecha(this.fechaFin));
  }

  private fillFinantiendasSelect() {
    this.locales = {
      advertencia: {
        required: 'Debe escoger al menos un local'
      },
      data: {
        value: this.localKey,
        isRequired: true,
        hasPlaceholder: true,
        label: { hasLabel: true, texto: 'Seleccione local' },
        id: '',
        clase: '',
        placeholder: 'Ingresar local',
        options: [
          {
            Finantienda_key: '091',
            Finantienda: 'PRO ATE',
          },
          {
            Finantienda_key: '092',
            Finantienda: 'PRUEBA 2'
          }
        ],
        selectedItems: [
          {
            CodigoCAI: '092',
            Finantienda: 'PRO ATE',
          }
        ]
      }
    }
  }

  actualizarFechaInicio(fechaInicio) {
    localStorage.setItem('fecha_inicio', fechaInicio);
    this.renderizar();
  }

  actualizarFechaFin(fechaFin) {
    localStorage.setItem('fecha_fin', fechaFin);
    this.renderizar();
  }

  async ngOnInit() {
    this.loading = true;
    this.fillFinantiendasSelect();
    if (localStorage.getItem('local') !== undefined && localStorage.getItem('local') !== null) {
      this.tituloInfo = JSON.parse(localStorage.getItem('local')).local;
      this.localKey = JSON.parse(localStorage.getItem('local')).local_key
    } else {
      this.localKey = 134;
      localStorage.setItem('local', JSON.stringify({ local: 'VEA Salaverry', local_key: 134 }));
    }
    await this.renderizar(true);
    this.loading = false;
  }
  async renderizar(primero = false) {
    this.loadingGraficos = true;
    this.porcentajeEntregadas = await this.tarjetas_service.obtenerPorcentajeEntregadas('' + this.localKey, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin'));
    const local: string[] = JSON.parse(localStorage.getItem('local')).local.split(' ')
    if (local[0].toLowerCase().includes('oec')) {
      const oeschle_participacion = await this.oeschle_service.obtener_porcentaje_participacion('' + this.localKey, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin'));
      this.porcentaje_oeschle_superior = oeschle_participacion.cumplimiento;
      if (
        oeschle_participacion.cumplimiento == undefined ||
        oeschle_participacion.cumplimiento == null
      ) {
        this.mostrar_oeschle = false;
      } else {
        const oeschle_plazo = await this.oeschle_service.obtener_porcentaje_plazo('' + this.localKey, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin'));
        this.porcentaje_oeschle_inferior = oeschle_plazo.cumplimiento;
        if (
          oeschle_plazo.cumplimiento == undefined ||
          oeschle_plazo.cumplimiento == null
        ) {
          this.mostrar_oeschle = false;
          this.mensaje_oec = 'Sin data para esta tienda en el rango de fecha seleccionado'
        } else {
          this.mostrar_oeschle = true;
        }
      }
    } else {
      this.mostrar_oeschle = false;
      this.mensaje_oec = 'Tiene que seleccionar una tienda oeschle'
    }

    if (local[0].toLowerCase().includes('vea')) {
      let crediventas = await this.crediventas_service.obtenerCrediventasGrafico('' + this.localKey, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin'));
      if (crediventas !== undefined && crediventas !== null && crediventas !== 0) {
        this.porcentajeCrediventas = crediventas.participacion_total;
        this.noPorcentajeCrediventas = crediventas.plazo_total;
        this.mostrar_crediventas = true;
      } else {
        this.mostrar_crediventas = false;
        this.mensaje_vea = 'Sin data para esta tienda en el rango de fecha seleccionado'
      }
    } else {
      this.mostrar_crediventas = false;
      this.mensaje_vea = 'Tiene que seleccionar una tienda VEA'
    }
    setTimeout(() => {
      localStorage.setItem('porcentajeEntregadas', '' + this.porcentajeEntregadas);
    }, 10);
    if (primero) {
      this.locales.data.options = await this.localService.obtenerLocales();
    }

    this.loadingGraficos = false;
  }

  actualizarLocal(local: any) {
    localStorage.setItem('local', JSON.stringify(local));
    this.localKey = local.local_key;
    this.tituloInfo = local.local;
    this.renderizar();
  }

  private darFormatoAnioMesDia(fecha: string): { anio: number, mes: number, dia: number } {
    if (
      fecha !== null && fecha !== undefined && fecha.length >= 8
    ) {
      const fin_anio = fecha.length - 4;
      const anio = parseInt(fecha.slice(0, fin_anio))
      const mes = parseInt(fecha.slice(fin_anio, fin_anio + 2))
      const dia = parseInt(fecha.slice(fin_anio + 2, fin_anio + 4))
      return {
        anio: anio,
        mes: mes,
        dia: dia
      }
    } else {
      return null
    }
  }

  private darFormatoFecha(fecha: Date): any {
    if (fecha != null) {
      const año = fecha.getFullYear();
      let mes = '';
      if (fecha.getMonth() + 1 < 10) {
        mes = '0' + (fecha.getMonth() + 1);
      } else {
        mes = '' + (fecha.getMonth() + 1);
      }
      let dia = '';

      if (fecha.getUTCDate() < 10) {
        dia = '0' + fecha.getUTCDate();
      } else {
        dia = '' + fecha.getUTCDate();
      }
      const nuevaFecha = año + mes + dia;
      return nuevaFecha;
    } else {
      return '';
    }

  }

}
