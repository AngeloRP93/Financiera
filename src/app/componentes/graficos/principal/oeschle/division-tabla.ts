import { OeschleTabla } from "./oeschle-tabla";
import { Location } from "@angular/common";

export class TablaConDivision extends OeschleTabla {
    divisiones_data = [];
    division_actual = null;
    divisiones;

    constructor(
        protected location: Location
    ) {
        super(location);
    }

    actualizarDivision(division:any){
        this.division_actual = division.id;
        this.renderizar();
    }

    fillDivisiones(data:{id:any,nombre:string}[]){
        this.divisiones = {
            advertencia: {
              required: 'Debe escoger una división'
            },
            data: {
              value: data[0].id,
              isRequired: true,
              hasPlaceholder: true,
              label: { hasLabel: true, texto: 'Escoger División' },
              id: '',
              clase: '',
              placeholder: 'Ingresar División',
              options: data,
              selectedItems: []
            }
          }
          this.division_actual = data[0].id; 
    }

    renderizar(){}
}