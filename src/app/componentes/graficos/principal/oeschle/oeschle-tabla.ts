import { TablaEnGrafico } from '../tablas-grafico';
import { PorcentajeOeschleResultado } from '../../../../services/oeschle-services/oeschle.service';
import { Location } from '@angular/common';
export class OeschleTabla extends TablaEnGrafico {
    totales:PorcentajeOeschleResultado
    constructor(protected location:Location) {
        super(location);
    }
}