import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OeschleRoutingModule } from './oeschle-routing.module';
import { PieGraficosModule } from '../../../../componentes-compartidos/pie-graficos/pie-graficos.module';
import { InfoModule } from '../../../../componentes-compartidos/info/info.module';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { OeschleComponent } from './oeschle.component';
import { OeschleServicesModule } from 'src/app/services/oeschle-services/oeschle-services.module';
import { MatButtonModule} from '@angular/material';
import { ParticipacionJcComponent } from './jefes_comerciales/participacion-jc/participacion-jc.component';
import { PlazoJcComponent } from './jefes_comerciales/plazo-jc/plazo-jc.component';
import { ParticipacionCComponent } from './cajeros/participacion-c/participacion-c.component';
import { PlazoCComponent } from './cajeros/plazo-c/plazo-c.component';
import { ParticipacionVComponent } from './vendedores/participacion-v/participacion-v.component';
import { PlazoVComponent } from './vendedores/plazo-v/plazo-v.component';
import { ParticipacionJcDetalleComponent } from './jefes_comerciales/participacion-jc/participacion-jc-detalle/participacion-jc-detalle.component';
import { PlazoJcDetalleComponent } from './jefes_comerciales/plazo-jc/plazo-jc-detalle/plazo-jc-detalle.component';
import { RouterModule } from '@angular/router';
import { InfoExtraComponent } from './info-extra/info-extra.component';
import { SelectsModule } from '../../../../componentes-compartidos/forms/form-content/selects/selects.module';

@NgModule({
  imports: [
    CommonModule,
    OeschleRoutingModule,
    PieGraficosModule,
    InfoModule,
    MatTableModule,
    MatSortModule,
    MatButtonModule,
    SelectsModule,
    OeschleServicesModule,
    RouterModule
  ],
  declarations: [OeschleComponent, ParticipacionJcComponent, PlazoJcComponent, ParticipacionCComponent, PlazoCComponent, ParticipacionVComponent, PlazoVComponent, ParticipacionJcDetalleComponent, PlazoJcDetalleComponent, InfoExtraComponent]
})
export class OeschleModule { }
