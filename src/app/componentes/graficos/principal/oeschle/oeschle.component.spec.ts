import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OeschleComponent } from './oeschle.component';

describe('OeschleComponent', () => {
  let component: OeschleComponent;
  let fixture: ComponentFixture<OeschleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OeschleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OeschleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
