import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-info-extra',
  templateUrl: './info-extra.component.html',
  styleUrls: ['./info-extra.component.scss']
})
export class InfoExtraComponent implements OnInit {
  @Input() titulo: string;
  @Input() subtitulo: string;
  constructor() { }

  ngOnInit() {
  }

}
