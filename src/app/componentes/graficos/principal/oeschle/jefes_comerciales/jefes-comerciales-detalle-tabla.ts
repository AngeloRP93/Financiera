import { TablaConDivision } from "../division-tabla";
import { ActivatedRoute } from '@angular/router';
import { PorcentajeOeschleResultado } from '../../../../../services/oeschle-services/oeschle.service';
import { Location } from "@angular/common";


export class JefesComercialesDetalleTabla extends TablaConDivision {
    jefe_comercial_id;
    jefe_comercial_nombre;
    totales:PorcentajeOeschleResultado;
    constructor(
        protected location: Location,
        protected route: ActivatedRoute
    ) {
        super(location);
        this.displayedColumns = ['Desc_Depart', 'real', 'meta', 'cumplimiento'];
        this.jefe_comercial_nombre = localStorage.getItem('jefe_comercial')
        this.route.params.subscribe(
            p => {
                this.jefe_comercial_id = p['id']
            });
    }
}