import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JefesComercialesTabla } from '../jefes-comerciales-tabla';
import { JefesComercialesService } from '../../../../../../services/oeschle-services/jefes-comerciales.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-participacion-jc',
  templateUrl: './participacion-jc.component.html',
  styleUrls: [
    './participacion-jc.component.scss',
    '../../../graficos.scss'
  ]
})
export class ParticipacionJcComponent extends JefesComercialesTabla implements OnInit {
  porcentaje_participacion: number;
  constructor(
    protected location:Location,
    protected router: Router,
    protected route: ActivatedRoute,
    private jefes_comerciales_service: JefesComercialesService
  ) {
    super(location,route, router);
  }

  async ngOnInit() {
    let jefes_comerciales_resultado = this.jefes_comerciales_service.obtener_participacion_jefes_comerciales(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin')).then(
      (data)=>{
        this.data = data
      }
    )
    let footer = this.jefes_comerciales_service.obtener_footer_participacion(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin')).then(
      (totales)=>{this.totales = totales}
    );
    Promise.all([jefes_comerciales_resultado,footer]).then( 
      ()=> {
        this.loading = false
      }
    )
  }

}
