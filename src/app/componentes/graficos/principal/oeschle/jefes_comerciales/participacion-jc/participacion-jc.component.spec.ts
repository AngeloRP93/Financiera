import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipacionJcComponent } from './participacion-jc.component';

describe('ParticipacionJcComponent', () => {
  let component: ParticipacionJcComponent;
  let fixture: ComponentFixture<ParticipacionJcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipacionJcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipacionJcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
