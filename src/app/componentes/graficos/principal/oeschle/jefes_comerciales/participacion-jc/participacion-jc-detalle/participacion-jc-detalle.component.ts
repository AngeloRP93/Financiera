import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JefesComercialesDetalleTabla } from '../../jefes-comerciales-detalle-tabla';
import { JefesComercialesService } from 'src/app/services/oeschle-services/jefes-comerciales.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-participacion-jc-detalle',
  templateUrl: './participacion-jc-detalle.component.html',
  styleUrls: [
    './participacion-jc-detalle.component.scss',
    '../../../../graficos.scss',
    '../../../division.scss'
  ]
})
export class ParticipacionJcDetalleComponent extends JefesComercialesDetalleTabla implements OnInit {
  constructor(
    protected location: Location,
    protected route: ActivatedRoute,
    private jefes_comerciales_service: JefesComercialesService,

  ) {
    super(location, route);
  }

  async ngOnInit() {
    this.divisiones_data = await this.jefes_comerciales_service.obtener_divisiones_part(this.jefe_comercial_id);
    this.fillDivisiones(this.divisiones_data)
    await this.renderizar();
    
  }


  renderizar() {
    this.data = null;
    let footer = this.jefes_comerciales_service.obtener_participacion_jefes_comerciales_division_porcentaje(this.jefe_comercial_id, this.division_actual).then(
      (totales) => {
        this.totales = totales;
      }
    )
    let jefes_resultado = this.jefes_comerciales_service.obtener_participacion_jefes_comerciales_division(this.jefe_comercial_id, this.division_actual).then(
      (jefes_comerciales)=> {
        this.data = jefes_comerciales
      }
    );

    Promise.all([jefes_resultado,footer]).then( 
      ()=> {
        this.loading = false
      }
    )
  }


}
