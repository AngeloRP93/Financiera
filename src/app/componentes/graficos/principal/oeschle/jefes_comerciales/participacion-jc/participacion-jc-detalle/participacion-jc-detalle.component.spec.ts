import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipacionJcDetalleComponent } from './participacion-jc-detalle.component';

describe('ParticipacionJcDetalleComponent', () => {
  let component: ParticipacionJcDetalleComponent;
  let fixture: ComponentFixture<ParticipacionJcDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipacionJcDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipacionJcDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
