import { OeschleTabla } from '../oeschle-tabla';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
export class JefesComercialesTabla extends OeschleTabla {

    constructor(
        protected location:Location,
        protected route: ActivatedRoute,
        protected router: Router
    ) {
        super(location);
        this.displayedColumns = ['jefe_comercial', 'real', 'meta', 'cumplimiento'];
    }


    haciaDetalle(jefe_comercial: any) {
        localStorage.setItem('jefe_comercial',jefe_comercial.jefe_comercial);
        this.router.navigate(['' + jefe_comercial.Cod_jefe_Comercial], { relativeTo: this.route });
    }
}