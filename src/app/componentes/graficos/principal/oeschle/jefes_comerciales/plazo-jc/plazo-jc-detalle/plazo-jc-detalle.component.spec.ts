import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlazoJcDetalleComponent } from './plazo-jc-detalle.component';

describe('PlazoJcDetalleComponent', () => {
  let component: PlazoJcDetalleComponent;
  let fixture: ComponentFixture<PlazoJcDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlazoJcDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlazoJcDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
