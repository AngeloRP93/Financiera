import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlazoJcComponent } from './plazo-jc.component';

describe('PlazoJcComponent', () => {
  let component: PlazoJcComponent;
  let fixture: ComponentFixture<PlazoJcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlazoJcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlazoJcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
