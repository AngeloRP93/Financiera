import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { OeschleService } from 'src/app/services/oeschle-services/oeschle.service';
import { JefesComercialesTabla } from '../jefes-comerciales-tabla';
import { JefesComercialesService } from '../../../../../../services/oeschle-services/jefes-comerciales.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-plazo-jc',
  templateUrl: './plazo-jc.component.html',
  styleUrls: [
    './plazo-jc.component.scss',
    '../../../graficos.scss'
  ]
})
export class PlazoJcComponent extends JefesComercialesTabla implements OnInit {
  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    protected location: Location,
    private jefes_comerciales_service: JefesComercialesService
  ) {
    super(location, route, router);
  }

  ngOnInit() {
    let jefes_comerciales_resultado = this.jefes_comerciales_service.obtener_plazo_jefes_comerciales(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin')).then(
      (data) => {
        this.data = data
      }
    )
    let footer = this.jefes_comerciales_service.obtener_footer_plazo(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin')).then(
      (totales) => { this.totales = totales }
    );
    Promise.all([jefes_comerciales_resultado, footer]).then(
      () => {
        this.loading = false
      }
    )
  }

}
