import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OeschleComponent } from './oeschle.component';
import { ParticipacionJcComponent } from './jefes_comerciales/participacion-jc/participacion-jc.component';
import { PlazoJcComponent } from './jefes_comerciales/plazo-jc/plazo-jc.component';
import { ParticipacionVComponent } from './vendedores/participacion-v/participacion-v.component';
import { PlazoVComponent } from './vendedores/plazo-v/plazo-v.component';
import { ParticipacionCComponent } from './cajeros/participacion-c/participacion-c.component';
import { PlazoCComponent } from './cajeros/plazo-c/plazo-c.component';
import { ParticipacionJcDetalleComponent } from './jefes_comerciales/participacion-jc/participacion-jc-detalle/participacion-jc-detalle.component';
import { PlazoJcDetalleComponent } from './jefes_comerciales/plazo-jc/plazo-jc-detalle/plazo-jc-detalle.component';

const routes: Routes = [
  {
    path: '',
    component: OeschleComponent
  },
  {
    path: 'jefes_comerciales/participacion',
    component: ParticipacionJcComponent
  },
  {
    path: 'jefes_comerciales/participacion/:id',
    component: ParticipacionJcDetalleComponent
  },
  {
    path: 'jefes_comerciales/plazo',
    component: PlazoJcComponent
  },
  {
    path: 'jefes_comerciales/plazo/:id',
    component: PlazoJcDetalleComponent
  },
  {
    path: 'vendedores/participacion',
    component: ParticipacionVComponent
  },
  {
    path: 'vendedores/plazo',
    component: PlazoVComponent
  },
  {
    path: 'cajeros/participacion',
    component: ParticipacionCComponent
  },
  {
    path: 'cajeros/plazo',
    component: PlazoCComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OeschleRoutingModule { }
