import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { OeschleService } from 'src/app/services/oeschle-services/oeschle.service';
import { Location } from '@angular/common';
import { FieldsetSelect } from '../../../../componentes-compartidos/forms/form-content/fieldset/fieldset.interface';
import { TypeSelect } from '../../../../componentes-compartidos/forms/form-content/selects/enum-type-select';
import { SelectInterface } from 'src/app/componentes-compartidos/forms/form-content/selects/select.interface';

@Component({
  selector: 'app-oeschle',
  templateUrl: './oeschle.component.html',
  styleUrls: ['./oeschle.component.scss']
})
export class OeschleComponent implements OnInit {
  titulo: string;
  grupos: FieldsetSelect;
  grupo_actual = 0;
  constructor(
    private oeschle_services: OeschleService,
    private router: Router,
    private location: Location,
    private route: ActivatedRoute
  ) {
    if (
      localStorage.getItem('grupo_escogido') !== undefined &&
      localStorage.getItem('grupo_escogido') !== null
    ) {
      this.grupo_actual = parseInt(localStorage.getItem('grupo_escogido'))
    }
  }

  async ngOnInit() {
    this.titulo = JSON.parse(localStorage.getItem('local')).local;

    const grupos_data = [
      {
        id: 0,
        nombre: 'Jefes Comerciales'
      },
      {
        id: 1,
        nombre: 'Vendedores'
      },
      {
        id: 2,
        nombre: 'Cajeros'
      }
    ];
    this.grupos = new FieldsetSelect(
      TypeSelect.select,
      new SelectInterface(
        '',
        { hasLabel: true, texto: 'Escoger Grupo' },
        this.grupo_actual,
        'text',
        true,
        true,
        '',
        grupos_data
      ),
      '',
      {
        required: 'Debe escoger un grupo'
      },
      { apariencia: 'outline' }
    )
  }

  regresar() {
    this.location.back();
  }


  actualizarGrupo(grupo: any) {
    this.grupo_actual = grupo.id
    localStorage.setItem('grupo_escogido', this.grupo_actual + '')
  }

  haciaParticipacion() {
    switch (this.grupo_actual) {
      case 0:
        this.router.navigate(['jefes_comerciales/participacion'], { relativeTo: this.route });
        break;
      case 1:
        this.router.navigate(['vendedores/participacion'], { relativeTo: this.route });
        break;
      case 2:
        this.router.navigate(['cajeros/participacion'], { relativeTo: this.route });
        break;
      default:
        break;
    }
  }

  haciaPlazo() {
    switch (this.grupo_actual) {
      case 0:
        this.router.navigate(['jefes_comerciales/plazo'], { relativeTo: this.route });
        break;
      case 1:
        this.router.navigate(['vendedores/plazo'], { relativeTo: this.route });
        break;
      case 2:
        this.router.navigate(['cajeros/plazo'], { relativeTo: this.route });
        break;
      default:
        break;
    }
  }

}
