import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlazoCComponent } from './plazo-c.component';

describe('PlazoCComponent', () => {
  let component: PlazoCComponent;
  let fixture: ComponentFixture<PlazoCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlazoCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlazoCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
