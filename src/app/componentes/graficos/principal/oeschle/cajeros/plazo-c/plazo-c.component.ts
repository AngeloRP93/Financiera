import { Component, OnInit } from '@angular/core';
import { OeschleTabla } from '../../oeschle-tabla';
import { Router } from '@angular/router';
import { CajerosService } from '../../../../../../services/oeschle-services/cajeros.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-plazo-c',
  templateUrl: './plazo-c.component.html',
  styleUrls: [
    './plazo-c.component.scss',
    '../../../graficos.scss'
  ]
})
export class PlazoCComponent extends OeschleTabla implements OnInit {

  constructor(
    protected location: Location,
    private cajeros_service: CajerosService
  ) {
    super(location)
    this.displayedColumns = ['Nombre_cajero', 'real', 'meta', 'cumplimiento'];
  }

  ngOnInit() {
    let cajeros_resultado = this.cajeros_service.obtener_plazo_cajeros(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin')).then(
      (cajeros)=> {
        this.data = cajeros;
      }
    );
    let footer = this.cajeros_service.obtener_footer_participacion(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin')).then(
      (totales)=> {
        this.totales = totales
      }
    );
    Promise.all([cajeros_resultado,footer]).then( 
      ()=> {
        this.loading = false
      }
    )
    
  }

}
