import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipacionCComponent } from './participacion-c.component';

describe('ParticipacionCComponent', () => {
  let component: ParticipacionCComponent;
  let fixture: ComponentFixture<ParticipacionCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipacionCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipacionCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
