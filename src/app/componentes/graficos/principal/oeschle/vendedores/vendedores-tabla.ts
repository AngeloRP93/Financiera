import { TablaConDivision } from '../division-tabla';
import { Location } from '@angular/common';
export class VendedoresTabla extends TablaConDivision {

    constructor(
        protected location: Location
    ) {
        super(location);
        this.displayedColumns = ['Nombre_Vendedor', 'real', 'meta', 'cumplimiento'];
    }
}