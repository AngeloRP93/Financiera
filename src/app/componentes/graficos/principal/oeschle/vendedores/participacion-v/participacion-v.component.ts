import { Component, OnInit } from '@angular/core';
import { VendedoresTabla } from '../vendedores-tabla';
import { VendedoresService } from 'src/app/services/oeschle-services/vendedores.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-participacion-v',
  templateUrl: './participacion-v.component.html',
  styleUrls: [
    './participacion-v.component.scss',
    '../../../graficos.scss',
    '../../division.scss'
  ]
})
export class ParticipacionVComponent extends VendedoresTabla implements OnInit {

  constructor(
    protected location: Location,
    private vendedores_service: VendedoresService
  ) {
    super(location);
  }

  async ngOnInit() {
    this.divisiones_data = await this.vendedores_service.obtener_divisiones(JSON.parse(localStorage.getItem('local')).local_key);
    this.fillDivisiones(this.divisiones_data)
    this.renderizar();
  }

  renderizar() {
    this.data = null;
    let vendedores_resultado = this.vendedores_service.obtener_participacion_vendedores(JSON.parse(localStorage.getItem('local')).local_key, this.division_actual, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin')).then(
      (vendedores) => {
        this.data = vendedores
      }
    );
    let footer = this.vendedores_service.obtener_footer_participacion(JSON.parse(localStorage.getItem('local')).local_key, this.division_actual, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin')).then(
      (totales) => {
        this.totales = totales;
      }
    );

    Promise.all([vendedores_resultado, footer]).then(
      () => {
        this.loading = false
      }
    )
  }

}
