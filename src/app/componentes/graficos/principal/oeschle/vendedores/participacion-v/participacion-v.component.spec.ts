import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipacionVComponent } from './participacion-v.component';

describe('ParticipacionVComponent', () => {
  let component: ParticipacionVComponent;
  let fixture: ComponentFixture<ParticipacionVComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipacionVComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipacionVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
