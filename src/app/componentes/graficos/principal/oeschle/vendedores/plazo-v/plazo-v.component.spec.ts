import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlazoVComponent } from './plazo-v.component';

describe('PlazoVComponent', () => {
  let component: PlazoVComponent;
  let fixture: ComponentFixture<PlazoVComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlazoVComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlazoVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
