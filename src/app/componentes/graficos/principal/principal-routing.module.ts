import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrincipalComponent } from './principal/principal.component';

const routes: Routes = [
  {
    path: '',
    component: PrincipalComponent
  },
  {
    path: 'tarjetas',
    loadChildren: '../../../../../src/app/componentes/graficos/principal/tarjetas/tarjetas.module#TarjetasModule'
  },
  {
    path: 'crediventas',
    loadChildren: '../../../../../src/app/componentes/graficos/principal/crediventas/crediventas.module#CrediventasModule'
  },
  {
    path:'oeschle',
    loadChildren: '../../../../../src/app/componentes/graficos/principal/oeschle/oeschle.module#OeschleModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrincipalRoutingModule { }
