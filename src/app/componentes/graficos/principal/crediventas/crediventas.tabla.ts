import { TablaEnGrafico } from "../tablas-grafico";
import { Location } from "@angular/common";

export class CrediventasTabla extends TablaEnGrafico {
    constructor(protected location:Location) {
        super(location);
    }

    getTotalVentas(tipo: string): number {
        // return this.data.map(t => t.venta).reduce((acc, value) => acc + value, 0);
        return parseFloat(JSON.parse(localStorage.getItem(tipo)).venta_total);
    }

    getTotalPlazo(tipo: string): number {
        return parseFloat(JSON.parse(localStorage.getItem(tipo)).plazo_total);
    }

    getTotalParticipacion(tipo: string): number {
        return parseFloat(JSON.parse(localStorage.getItem(tipo)).participacion_total);
    }
}