import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { CrediventasTabla } from '../crediventas.tabla';
import { CrediventasService } from '../../../../../services/crediventas-services/crediventas.service';
import { ElectroInterface } from './electro.interface';
import { Location } from '@angular/common';
@Component({
  selector: 'app-electro',
  templateUrl: './electro.component.html',
  styleUrls: ['./electro.component.scss', '../../graficos.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ElectroComponent extends CrediventasTabla implements OnInit {
  data: ElectroInterface[] = [];
  constructor(
    protected location: Location,
    private crediventasService: CrediventasService) {
    super(location);
    this.displayedColumns = ['nombre_vendedor', 'venta', 'plazo', 'participacion',];
  }

  async ngOnInit() {
    this.data = await this.crediventasService.obtenerElectroData(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin'));
    this.loading = false;
  }

}
