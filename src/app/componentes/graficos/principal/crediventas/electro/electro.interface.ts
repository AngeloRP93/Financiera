export interface ElectroInterface {
    nombre_vendedor: string;
    venta: number;
    participacion: number;
    plazo: number;
    COMISION_CUOTA_1_0: number;
    COMISION_CUOTA_2_3: number;
    COMISION_CUOTA_4_6: number;
    COMISION_CUOTA_7_12: number;
    COMISION_CUOTA_13_MAS: number;
}