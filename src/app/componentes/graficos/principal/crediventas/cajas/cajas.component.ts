import { Component, OnInit } from '@angular/core';
import { CrediventasTabla } from '../crediventas.tabla';
import { CrediventasService } from '../../../../../services/crediventas-services/crediventas.service';
import { CajeroInterface } from './cajero.interface';
import { Location } from '@angular/common';

@Component({
  selector: 'app-cajas',
  templateUrl: './cajas.component.html',
  styleUrls: ['./cajas.component.scss', '../../graficos.scss']
})
export class CajasComponent extends CrediventasTabla implements OnInit {
  data: CajeroInterface[] = [];
  constructor(
    protected location: Location,
    private crediventasService: CrediventasService) {
    super(location);
    this.displayedColumns = ['nombre_cajero', 'venta', 'plazo', 'participacion',];
  }

  async ngOnInit() {
    this.data = await this.crediventasService.obtenerNoElectroData(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin'));
    this.loading = false;
  }

}
