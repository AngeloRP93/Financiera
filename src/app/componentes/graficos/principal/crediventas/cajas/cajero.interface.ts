export interface CajeroInterface {
    nombre_cajero: string;
    venta: number;
    participacion: number;
    plazo: number;
}