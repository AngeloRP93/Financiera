import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrediventasComponent } from './crediventas/crediventas.component';
import { ElectroComponent } from './electro/electro.component';
import { CajasComponent } from './cajas/cajas.component';
import { CrediventasRoutingModule } from './crediventas-routing.module';
import { PieGraficosModule } from '../../../../componentes-compartidos/pie-graficos/pie-graficos.module';
import { InfoModule } from '../../../../componentes-compartidos/info/info.module';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { CrediventasServicesModule } from '../../../../services/crediventas-services/crediventas-services.module';

@NgModule({
  imports: [
    CommonModule,
    CrediventasRoutingModule,
    PieGraficosModule,
    InfoModule,
    MatTableModule,
    MatSortModule,
    CrediventasServicesModule
  ],
  declarations: [CrediventasComponent, ElectroComponent, CajasComponent]
})
export class CrediventasModule { }
