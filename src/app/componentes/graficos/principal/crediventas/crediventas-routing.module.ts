import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrediventasComponent } from './crediventas/crediventas.component';
import { ElectroComponent } from './electro/electro.component';
import { CajasComponent } from './cajas/cajas.component';

const routes: Routes = [
  {
    path: '',
    component: CrediventasComponent
  },
  {
    path: 'electro',
    component: ElectroComponent
  },
  {
    path: 'cajas',
    component: CajasComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrediventasRoutingModule { }