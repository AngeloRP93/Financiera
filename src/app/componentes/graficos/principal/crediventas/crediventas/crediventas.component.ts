import { Component, OnInit } from '@angular/core';
import { CrediventasService } from '../../../../../services/crediventas-services/crediventas.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-crediventas',
  templateUrl: './crediventas.component.html',
  styleUrls: ['./crediventas.component.scss']
})
export class CrediventasComponent implements OnInit {
  electro = {}
  cajas = {}
  loading = true;
  titulo: string;
  constructor(
    private crediventasServices: CrediventasService,
    private router: Router,
    private location: Location,
    private route: ActivatedRoute
  ) { }

  async ngOnInit() {
    this.titulo = JSON.parse(localStorage.getItem('local')).local;
    this.electro = await this.crediventasServices.obtenerElectroPorcentaje(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin'));
    localStorage.setItem('electro', JSON.stringify(this.electro));
    this.cajas = await this.crediventasServices.obtenerElectroNoPorcentaje(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin'));
    localStorage.setItem('cajas', JSON.stringify(this.cajas));
    this.loading = false;
  }

  regresar() {
    this.location.back();
  }

  haciaElectro() {
    this.router.navigate(['electro'], { relativeTo: this.route });
  }

  haciaCajas() {
    this.router.navigate(['cajas'], { relativeTo: this.route });
  }

}
