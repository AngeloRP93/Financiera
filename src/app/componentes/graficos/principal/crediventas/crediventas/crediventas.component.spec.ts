import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrediventasComponent } from './crediventas.component';

describe('CrediventasComponent', () => {
  let component: CrediventasComponent;
  let fixture: ComponentFixture<CrediventasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrediventasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrediventasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
