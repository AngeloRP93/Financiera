import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TarjetasRoutingModule } from './tarjetas-routing.module';
import { TarjetasComponent } from './tarjetas.component';
import { EntregadasComponent } from './entregadas/entregadas.component';
import { IngresadasComponent } from './ingresadas/ingresadas.component';
import { ActivadasComponent } from './activadas/activadas.component';
import { PieGraficosModule } from '../../../../componentes-compartidos/pie-graficos/pie-graficos.module';
import { TarjetasServicesModule } from '../../../../services/tarjetas/tarjetas.module';
import { InfoModule } from '../../../../componentes-compartidos/info/info.module';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
  imports: [
    CommonModule,
    TarjetasRoutingModule,
    PieGraficosModule,
    TarjetasServicesModule,
    InfoModule,
    MatTableModule,
    MatSortModule

  ],
  declarations: [
    TarjetasComponent,
    EntregadasComponent,
    IngresadasComponent,
    ActivadasComponent
  ]
})
export class TarjetasModule { }
