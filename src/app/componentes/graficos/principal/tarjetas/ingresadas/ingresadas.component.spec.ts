import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresadasComponent } from './ingresadas.component';

describe('IngresadasComponent', () => {
  let component: IngresadasComponent;
  let fixture: ComponentFixture<IngresadasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresadasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
