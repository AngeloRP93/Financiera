import { Component, OnInit } from '@angular/core';
import { TarjetasTabla } from '../tarjeta_tablas';
import { TarjetasService } from '../../../../../services/tarjetas/tarjetas.service';
import * as moment from 'moment';
import { Location } from '@angular/common';
@Component({
  selector: 'app-ingresadas',
  templateUrl: './ingresadas.component.html',
  styleUrls: ['./ingresadas.component.scss','../../graficos.scss']
})
export class IngresadasComponent extends TarjetasTabla implements OnInit {
  diasDelMes = 0;
  columnasTotal = ['finantienda', 'ingresado'];
  dataTotal = [];
  constructor(
    protected location:Location,
    private tarjetasService: TarjetasService
  ) {
    super(location);
    this.fillHeadersColumnas();
    // this.displayedColumns = ['ejecutivo', 'nro_tc_entregado', 'ppto_entregado_diario_acumulado', 'porcentaje_entregado'];
  }

  async ngOnInit() {
    this.dataTotal = [{ finantienda: this.titulo, ingresado: this.getPorcentaje()}];
    this.data = await this.tarjetasService.obtenerIngresadas(JSON.parse(localStorage.getItem('local')).local_key);
    this.loading = false;
  }

  getPorcentaje() {
    return parseFloat(localStorage.getItem('porcentajeIngresadas'));
  }

  private fillHeadersColumnas() {
    const date = new Date(), y = date.getFullYear(), m = date.getMonth();
    this.displayedColumns = ['ejecutivo'];
    this.diasDelMes = date.getDate();
    let inicioMes = new Date(y, m, 1);
    for (let index = 1; index <= this.diasDelMes; index++) {
      this.displayedColumns.push('' + moment(inicioMes).add(index - 1, 'days').format('YYYY-MM-DD'));
    }
    this.displayedColumns.push('Total');
  }


}
