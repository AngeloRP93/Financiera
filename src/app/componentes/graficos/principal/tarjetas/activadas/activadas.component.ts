import { Component, OnInit } from '@angular/core';
import { TarjetasTabla } from '../tarjeta_tablas';
import { TarjetasService } from '../../../../../services/tarjetas/tarjetas.service';
import { Location } from '@angular/common';
export interface TarjetasActivadas {
  ejecutivo: string;
  nro_tc_Activada: number;
  ppto_entregado_diario_acum: number;
  porcentaje_activado: number;
}
export interface TarjetasSupervisorActivadas {
  supervisor: string;
  nro_tc_activada_supervisor: number;
  ppto_activada_diario_acumulado_supervisor: number;
  porcentaje_activada: number;
}
@Component({
  selector: 'app-activadas',
  templateUrl: './activadas.component.html',
  styleUrls: ['./activadas.component.scss', '../../graficos.scss']
})
export class ActivadasComponent extends TarjetasTabla implements OnInit {
  data: TarjetasActivadas[] = [];
  supervisorData: TarjetasSupervisorActivadas[] = [];
  displayedSupervisorColumns: string[] = [];
  constructor(
    protected location:Location,
    private tarjetasService: TarjetasService
  ) {
    super(location);
    this.displayedColumns = ['ejecutivo', 'nro_tc_Activada', 'ppto_entregado_diario_acum', 'porcentaje_activado'];
    this.displayedSupervisorColumns = ['supervisor', 'nro_tc_activada_supervisor', 'ppto_activada_diario_acumulado_supervisor', 'porcentaje_activada'];
  }

  getTotalActivadas(): number {
    return this.data.map(t => t.nro_tc_Activada).reduce((acc, value) => acc + value, 0);
  }

  getTotalMetas(): number {
    return this.data.map(t => t.ppto_entregado_diario_acum).reduce((acc, value) => acc + value, 0);
  }

  getPorcentaje(): number {
    return parseFloat(localStorage.getItem('porcentajeActivadas'));
  }

  async ngOnInit() {
    this.data = await this.tarjetasService.obtenerActivadas(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin'));
    this.supervisorData = await this.tarjetasService.obtenerActivadasSupervisor(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin'));

    this.loading = false;
  }
}
