import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TarjetasComponent } from './tarjetas.component';
import { EntregadasComponent } from './entregadas/entregadas.component';
import { IngresadasComponent } from './ingresadas/ingresadas.component';
import { ActivadasComponent } from './activadas/activadas.component';

const routes: Routes = [
  {
    path: '',
    component: TarjetasComponent
  },
  {
    path: 'entregadas',
    component: EntregadasComponent
  },
  {
    path: 'ingresadas',
    component: IngresadasComponent
  },
  {
    path: 'activadas',
    component: ActivadasComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TarjetasRoutingModule { }
