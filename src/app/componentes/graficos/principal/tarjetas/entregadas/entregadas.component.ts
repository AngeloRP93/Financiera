import { Component, OnInit } from '@angular/core';
import { TarjetasService } from '../../../../../services/tarjetas/tarjetas.service';
import { TarjetasTabla } from '../tarjeta_tablas';
import { Location } from '@angular/common';
export interface TarjetasEntregadas {
  ejecutivo: string;
  nro_tc_entregado: number;
  ppto_entregado_diario_acumulado: number;
  porcentaje_entregado: number;
}
@Component({
  selector: 'app-entregadas',
  templateUrl: './entregadas.component.html',
  styleUrls: ['./entregadas.component.scss', '../../graficos.scss']
})
export class EntregadasComponent extends TarjetasTabla implements OnInit {
  data: TarjetasEntregadas[] = [];
  constructor(
    protected location:Location,
    private tarjetasService: TarjetasService
  ) {
    super(location);
    this.displayedColumns = ['ejecutivo', 'nro_tc_entregado', 'ppto_entregado_diario_acumulado', 'porcentaje_entregado'];
  }

  /** Gets the total cost of all tarjetasEntregadas. */
  getTotalEntregadas(): number {
    return this.data.map(t => t.nro_tc_entregado).reduce((acc, value) => acc + value, 0);
  }

  getTotalMetas(): number {
    return this.data.map(t => t.ppto_entregado_diario_acumulado).reduce((acc, value) => acc + value, 0);
  }

  getPorcentaje(): number {
    return parseFloat(localStorage.getItem('porcentajeEntregadas'));
  }

  async ngOnInit() {
    this.data = await this.tarjetasService.obtenerEntregadas(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin'));
    this.loading = false;
  }

}
