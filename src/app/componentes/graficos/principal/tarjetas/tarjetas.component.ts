import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TarjetasService } from '../../../../services/tarjetas/tarjetas.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.component.html',
  styleUrls: ['./tarjetas.component.scss']
})
export class TarjetasComponent implements OnInit {
  porcentajeEntregadas = 0;
  porcentajeIngresadas = 0;
  porcentajeActivadas = 0;
  titulo:string;
  loading = true;
  constructor(
    private tarjetasService: TarjetasService,
    private router: Router,
    private route:ActivatedRoute,
    private location:Location
  ) { }

  async ngOnInit() {

    if (localStorage.getItem('porcentajeEntregadas') !== undefined) {
      this.porcentajeEntregadas = parseInt(localStorage.getItem('porcentajeEntregadas'));
    } else {
      this.porcentajeEntregadas = await this.tarjetasService.obtenerEntregadas(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin'))
      localStorage.setItem('porcentajeEntregadas', '' + this.porcentajeEntregadas);
    }
    this.titulo = JSON.parse(localStorage.getItem('local')).local;
    this.porcentajeActivadas = await this.tarjetasService.obtenerPorcentajeActivadas(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin'))
    this.porcentajeIngresadas = await this.tarjetasService.obtenerPorcentajeIngresadas(JSON.parse(localStorage.getItem('local')).local_key, localStorage.getItem('fecha_inicio'), localStorage.getItem('fecha_fin'))
    setTimeout(() => {
      localStorage.setItem('porcentajeActivadas', '' + this.porcentajeActivadas);
      localStorage.setItem('porcentajeIngresadas', '' + this.porcentajeIngresadas);
    }, 1);
    this.loading = false;
  }

  regresar() {
    this.location.back();
  }

  haciaIngresadas() {
    this.router.navigate(['ingresadas'],{ relativeTo: this.route});
  }

  haciaEntregadas() {
    this.router.navigate(['entregadas'],{ relativeTo: this.route});
  }

  haciaActivadas() {
    this.router.navigate(['activadas'],{ relativeTo: this.route});
  }

}
