import { Location } from '@angular/common';
export class TablaEnGrafico {
    loading = true;
    titulo: string;
    displayedColumns: string[]
    data: any[];

    constructor(protected location:Location) {
        this.displayedColumns = [];
        this.data = [];
        this.titulo = JSON.parse(localStorage.getItem('local')).local;
    }

    getColor(valor: number): string {
        if (isNaN(valor)) {
            return '#757575';
        } else {
            if (valor < 40) {
                return '#f86964';
            } else {
                if (valor < 60) {
                    return '#6b82af';
                } else {
                    return '#39c663';
                }
            }
        }
    }

    getFinantienda() {
        return JSON.parse(localStorage.getItem('local')).local;
    }

    regresar() {
        this.location.back()
    }
}