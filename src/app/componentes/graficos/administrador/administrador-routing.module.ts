import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutAdministradorComponent } from './layout-administrador/layout-administrador.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutAdministradorComponent,
    children: [
      {
        path: '',
        redirectTo: 'usuarios'
      },
      {
        path:'usuarios',
        loadChildren: './usuarios/usuarios.module#UsuariosModule'
      },
      {
        path: 'graficos',
        loadChildren: 'src/app/componentes/graficos/principal/principal.module#PrincipalModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministradorRoutingModule { }
