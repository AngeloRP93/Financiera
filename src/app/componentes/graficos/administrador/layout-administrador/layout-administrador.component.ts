import { Component } from '@angular/core';
import { Layout } from '../../../../componentes-compartidos/layout-principal/layout/layout';
import { TipoElementoNavegacion } from '../../../../componentes-compartidos/layout-principal/layout/elementos/tipo-elemento-navegacion.enum';


@Component({
  selector: 'app-layout-administrador',
  templateUrl: './layout-administrador.component.html',
  styleUrls: ['./layout-administrador.component.scss']
})
export class LayoutAdministradorComponent extends Layout {

  constructor(
  ) {
    super('Administrador');
    this.data.barra_navegacion = {
      titulo: 'Financiera',
      subtitulo: this.usuario == undefined || this.usuario.email == null ? '':this.usuario.email,
      elementos: [
        {
          tipo: TipoElementoNavegacion['Sin Hijos'],
          icono: 'group',
          titulo: 'Usuarios',
          ruta: 'usuarios',
        },
        {
          tipo: TipoElementoNavegacion['Sin Hijos'],
          icono: 'pie_chart',
          titulo: 'Indicadores',
          ruta: 'graficos',
        }
      ]
    };
  }

}
