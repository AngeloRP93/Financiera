import { Component, OnInit, Inject } from '@angular/core';
import { Form } from '../../../../../componentes-compartidos/forms/form/form.clase';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Fieldset } from 'src/app/componentes-compartidos/forms/form-content/fieldset/fieldset.clase';
import { FieldsetContent } from 'src/app/componentes-compartidos/forms/form-content/fieldset/fieldset.interface';
import { TypeSelect } from '../../../../../componentes-compartidos/forms/form-content/selects/enum-type-select';
import * as _moment from 'moment';
import { NotificacionService } from '../../../../../services/notificacion.service';
import { UsuarioService } from '../../../../../services/usuarios-services/usuario.service';
import { RequestUsuario } from '../registrar_request.interface';
import { Rol } from '../../../../../services/usuarios-services/rol.service';

@Component({
  selector: 'app-registrar-usuario',
  templateUrl: './registrar-usuario.component.html',
  styleUrls: ['../popup-usuario.scss']
})
export class RegistrarUsuarioComponent implements OnInit {
  formulario: Form;
  contador = 0;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Rol[],
    private notificacion:NotificacionService,
    private userService:UsuarioService
  ) {
    this.formulario = new Form(
      'registrar_usuario',
      [
        new Fieldset(
          [
            new FieldsetContent(
              true,
              'email',
              null,
              'email',
              { hasLabel: true, texto: 'Cuenta de Usuario' },
              { required: 'Por favor escriba la cuenta de usuario' },
              { title: 'Escriba cuenta de usuario', apariencia: 'outline' }
            ),
            new FieldsetContent(
              true,
              'password',
              null,
              'password',
              { hasLabel: true, texto: 'Contraseña' },
              { required: 'Por favor escriba su contraseña' },
              { title: '', apariencia: 'outline' },
              true,
              true,
            ),
            new FieldsetContent(
              true,
              'nombres',
              null,
              'text',
              { hasLabel: true, texto: 'Nombres' },
              { required: 'Por favor escriba el nombre de usuario' },
              { title: '', apariencia: 'outline' },
              true,
              true,
            ),
            new FieldsetContent(
              true,
              'apellido_paterno',
              null,
              'text',
              { hasLabel: true, texto: 'Apellido Paterno' },
              { required: 'Por favor escriba el apellido paterno del usuario' },
              { title: '', apariencia: 'outline' },
              true,
              true,
            ),
            new FieldsetContent(
              true,
              'apellido_materno',
              null,
              'text',
              { hasLabel: true, texto: 'Apellido Materno' },
              { required: 'Por favor escriba el apellido materno del usuario' },
              { title: '', apariencia: 'outline' },
              true,
              true,
            ),
            new FieldsetContent(
              false,
              'rol_id',
              null,
              TypeSelect.select,
              { hasLabel: true, texto: 'Rol' },
              { required: 'Seleccione un rol' },
              { title: '', apariencia: 'outline' },
              true,
              true,
              { clase: '' },
              '',
              this.data.map( (rol)=>{ return {id:rol.rol_id,nombre:rol.nombre}})
            )
          
          ]
        )
      ]
    );
  }

  ngOnInit() {
  }

  private validacion(data_usuario: RequestUsuario): boolean {
    if (
      data_usuario.nombres !== undefined && data_usuario.nombres !== null && data_usuario.nombres.trim() !== '' &&
      data_usuario.password !== undefined && data_usuario.password !== null && data_usuario.password.trim() !== '' &&
      data_usuario.apellido_paterno !== undefined && data_usuario.apellido_paterno !== null && data_usuario.apellido_paterno.trim() !== '' &&
      data_usuario.apellido_materno !== undefined && data_usuario.apellido_materno !== null && data_usuario.apellido_materno.trim() !== '' &&
      data_usuario.rol_id !== undefined && data_usuario.rol_id !== null
    ) {
      return true;
    } else {
      return false;
    }
  }

  private registrar(event: RequestUsuario) {
    
    this.userService.registrar(event).then(() => {
      this.formulario.loading = false;
    });
  }

  async requestBack(event: RequestUsuario) {
      if (this.validacion(event)) {
        this.registrar(event);
      } else {
        this.notificacion.camposImcompletos();
      }

  }

}
