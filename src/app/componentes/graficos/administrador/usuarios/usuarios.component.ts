import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { TypeButton } from 'src/app/enums/type-button.enum';
import { UsuarioService } from '../../../../services/usuarios-services/usuario.service';
import { MatDialog, MatSort, MatPaginator, MatTableDataSource, MatTable } from '@angular/material';
import { takeWhile } from 'rxjs/operators';
import { EditarUsuarioComponent } from './editar-usuario/editar-usuario.component';
import { RegistrarUsuarioComponent } from './registrar-usuario/registrar-usuario.component';
import { MyButtonInterface } from '../../../../componentes-compartidos/buttons/button/button.interface';
import { Usuario } from '../../../../usuario.interface';
import { TipoEventoBack } from '../../../../enums/tipo_evento_back.enum';
import { EliminarUsuarioComponent } from './eliminar-usuario/eliminar-usuario.component';
import { RolService } from 'src/app/services/usuarios-services/rol.service';
import { Rol } from '../../../../services/usuarios-services/rol.service';
import { CambiarClaveComponent } from './cambiar-clave/cambiar-clave.component';
import { CambiarEstadoUsuarioComponent } from './cambiar-estado-usuario/cambiar-estado-usuario.component';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit, OnDestroy {
  activo = true;
  loading = true;
  numero_fila_actual: number;
  buttons: MyButtonInterface[];
  buttonsExt: MyButtonInterface[];
  data: Usuario[];
  roles: Rol[];
  displayedColumns: string[];
  dataSource: MatTableDataSource<any>;
  sortedData: any;
  @ViewChild(MatTable) table: MatTable<Usuario>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    public dialog: MatDialog,
    private user_service: UsuarioService,
    private rol_service: RolService
  ) {
    this.displayedColumns = ['acciones', 'estado', 'email', 'nombres', 'rol'];
    this.buttons = [
      {
        id: 'editar_usuario',
        class: 'editar',
        titulo: 'Editar Usuario',
        tooltipTitulo: 'Editar Usuario',
        imagen: 'edit',
        toolTipPosition: 'above',
        type: TypeButton.Icon,
        disabled: false,
        mostrar: function () { return true; }
      },
      {
        id: 'cambiar_clave',
        class: '',
        titulo: 'Cambiar clave',
        tooltipTitulo: 'Cambiar clave',
        imagen: 'lock',
        toolTipPosition: 'above',
        type: TypeButton.Icon,
        disabled: false,
        mostrar: function () { return true; }
      },
      {
        id: 'eliminar',
        class: '',
        titulo: 'Eliminar ',
        tooltipTitulo: 'Pulse para eliminar',
        imagen: 'delete_forever',
        toolTipPosition: 'right',
        type: TypeButton.Icon,
        disabled: false,
        mostrar: function (data: Usuario): boolean {
          if (data.rol_id === 1) { return false }
          else { return true }
        }
      }
    ];
    this.buttonsExt = [
      {
        id: 'registrar_usuario',
        class: 'editar',
        titulo: 'Registrar Usuario',
        tooltipTitulo: 'Pulse para mostrar el formulario de registro de usuario',
        imagen: 'person_add',
        toolTipPosition: 'above',
        type: TypeButton.Fab,
        disabled: false,
        mostrar: function () { return true; }
      }
    ];
    this.user_service.cambioEstado.pipe(takeWhile(() => this.activo)).subscribe(
      (evento) => {
        this.dialog.closeAll();
        switch (evento.tipo) {
          case TipoEventoBack.Registrar:
            this.data.push(evento.data)
            this.recargar_tabla()
            this.table.renderRows();
            break;
          case TipoEventoBack.Actualizar:
            if (this.data[this.numero_fila_actual]) {
              this.data[this.numero_fila_actual].apellido_materno = evento.data.apellido_materno;
              this.data[this.numero_fila_actual].apellido_paterno = evento.data.apellido_paterno;
              this.data[this.numero_fila_actual].nombres = evento.data.nombres;
              this.data[this.numero_fila_actual].estado = evento.data.estado;
              this.dataSource = new MatTableDataSource(this.data)
              this.table.renderRows();
            }
            break;
          default:
            this.data.splice(this.numero_fila_actual, 1)
            this.recargar_tabla()
            this.table.renderRows();
            break;
        }
      }
    );
  }

  ngOnDestroy() {
    this.activo = false;
  }

  ngOnInit() {
    if (this.loading === true) {
      this.rol_service.listar().then(
        (roles) => {
          this.roles = roles;
        }
      )
      this.render();
    }
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    // this.sortedData =  this.dataSource;
  }

  registrar() {
    this.dialog.open(RegistrarUsuarioComponent, {
      width: '600px',
      data: this.roles
    });
  }

  activar_usuario(data: Usuario, fila: number) {
    this.numero_fila_actual = fila;
    this.dialog.open(CambiarEstadoUsuarioComponent, {
      data: { usuario: data, activar: true }
    });
  }

  desactivar_usuario(data: Usuario, fila: number) {
    this.numero_fila_actual = fila;
    if (data.rol_nombre !== 'ADMIN') {
      this.dialog.open(CambiarEstadoUsuarioComponent, {
        data: { usuario: data, activar: false }
      });
    }
  }

  operation(data: { id: string, data: Usuario }, fila: number) {
    this.numero_fila_actual = fila;
    switch (data.id) {
      case 'editar_usuario':
        {
          this.dialog.open(EditarUsuarioComponent, {
            data: data.data
          });
          break;
        }
      case 'cambiar_clave':
        {
          this.dialog.open(CambiarClaveComponent, {
            data: data.data
          });
          break
        }
      default:
        {
          this.dialog.open(EliminarUsuarioComponent, {
            data: data.data
          });
          break;
        }
    }

  }

  private render() {
    this.user_service.listar().then(
      (usuarios) => {
        this.data = usuarios;
        this.recargar_tabla()
      }
    );
  }

  private recargar_tabla() {
    this.dataSource = new MatTableDataSource(this.data)
    this.loading = false;
    setTimeout(() => {
      if (this.paginator) {
        this.dataSource.paginator = this.paginator
      }
      if (this.sort) {
        this.dataSource.sort = this.sort
      }
    }, 100);
  }

}
