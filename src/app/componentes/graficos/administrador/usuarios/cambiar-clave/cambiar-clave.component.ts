import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalComponent } from '../../../../../componentes-compartidos/modals/modal/modal.component';
import { Usuario } from 'src/app/usuario.interface';
import { MAT_DIALOG_DATA } from '@angular/material';
import { UsuarioService } from 'src/app/services/usuarios-services/usuario.service';

@Component({
  selector: 'app-cambiar-clave',
  templateUrl: './cambiar-clave.component.html',
  styleUrls: ['./cambiar-clave.component.scss']
})
export class CambiarClaveComponent implements OnInit {
  form: FormGroup;
  hide = true;
  @ViewChild(ModalComponent) modal: ModalComponent
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Usuario,
    private _formBuilder: FormBuilder,
    private user_service: UsuarioService
  ) {
    this.form = this._formBuilder.group({
      'password': ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  contrasenaValida(): boolean {
    return this.form.get('password').valid;
  }
  getErrorMessage(): string {
    return 'Este campo es requerido';
  }

  cerrar() {
    this.modal.onCloseCancel();
  }

  aceptar() {
    if (this.form.valid) {
      this.modal.onCloseCancel();
      this.user_service.cambiar_clave(this.data.usuario_id, this.form.get('password').value)
    }

  }

}
