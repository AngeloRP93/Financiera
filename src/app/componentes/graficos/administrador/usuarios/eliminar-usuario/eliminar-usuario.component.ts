import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Usuario } from '../../../../../usuario.interface';
import { MAT_DIALOG_DATA } from '@angular/material';
import { UsuarioService } from '../../../../../services/usuarios-services/usuario.service';
import { ModalComponent } from '../../../../../componentes-compartidos/modals/modal/modal.component';

@Component({
  selector: 'app-eliminar-usuario',
  templateUrl: './eliminar-usuario.component.html',
  styleUrls: ['./eliminar-usuario.component.scss']
})
export class EliminarUsuarioComponent implements OnInit {
  @ViewChild(ModalComponent) modal: ModalComponent
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Usuario,
    private user_service: UsuarioService
  ) { }

  ngOnInit() {
  }

  cerrar() {
    this.modal.onCloseCancel();
  }

  eliminar() {
    this.user_service.eliminar(this.data.usuario_id)
  }

}
