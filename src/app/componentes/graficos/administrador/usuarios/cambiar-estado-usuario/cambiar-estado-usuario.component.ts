import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ModalComponent } from '../../../../../componentes-compartidos/modals/modal/modal.component';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Usuario } from '../../../../../usuario.interface';
import { UsuarioService } from '../../../../../services/usuarios-services/usuario.service';

@Component({
  selector: 'app-cambiar-estado-usuario',
  templateUrl: './cambiar-estado-usuario.component.html',
  styleUrls: ['./cambiar-estado-usuario.component.scss']
})
export class CambiarEstadoUsuarioComponent implements OnInit {
  @ViewChild(ModalComponent) modal: ModalComponent
  titulo = ' Usuario'
  info: { titulo: string, class: string, id: string }
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { usuario: Usuario, activar: boolean },
    private user_service: UsuarioService
  ) {
    if (data.activar) {
      this.titulo = 'Activar'
    } else {
      this.titulo = 'Desactivar'
    }
    this.info = {
      titulo: this.titulo + ' Usuario',
      class: '',
      id: 'cambiar_estado_usuario'
    }
  }

  ngOnInit() {
  }

  aceptar() {
    this.user_service.cambiar_estado(this.data)
  }

  cerrar() {
    this.modal.onCloseCancel();
  }

}
