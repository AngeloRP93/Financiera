import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CambiarEstadoUsuarioComponent } from './cambiar-estado-usuario.component';

describe('CambiarEstadoUsuarioComponent', () => {
  let component: CambiarEstadoUsuarioComponent;
  let fixture: ComponentFixture<CambiarEstadoUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CambiarEstadoUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CambiarEstadoUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
