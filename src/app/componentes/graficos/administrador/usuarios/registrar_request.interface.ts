export interface RequestUsuario {
    apellido_paterno: string;
    apellido_materno: string;
    nombres: string;
    email: string;
    password: string;
    rol_id: number

}
