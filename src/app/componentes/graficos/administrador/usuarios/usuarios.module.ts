import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuariosComponent } from './usuarios.component';
import { EditarUsuarioComponent } from './editar-usuario/editar-usuario.component';
import { RegistrarUsuarioComponent } from './registrar-usuario/registrar-usuario.component';
import { EliminarUsuarioComponent } from './eliminar-usuario/eliminar-usuario.component';
import { LoadingsModule } from '../../../../componentes-compartidos/loadings/loadings.module';
import { UsuariosServicesModule } from '../../../../services/usuarios-services/usuarios-services.module';
import { ModalsModule } from '../../../../componentes-compartidos/modals/modals.module';
import { SharedFormsModule } from '../../../../componentes-compartidos/forms/forms.module';
import { MatSortModule, MatTableModule, MatPaginatorModule, MatPaginatorIntl, MatButtonModule, MatInputModule, MatIconModule } from '@angular/material';
import { ButtonsModule } from '../../../../componentes-compartidos/buttons/buttons.module';
import { BuscadorInputModule } from '../../../../componentes-compartidos/mat-tables/buscador-input/buscador-input.module';
import { MatPaginatorIntlCro } from '../../../../componentes-compartidos/mat-tables/custom.paginator';
import { CambiarClaveComponent } from './cambiar-clave/cambiar-clave.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { CambiarEstadoUsuarioComponent } from './cambiar-estado-usuario/cambiar-estado-usuario.component';

@NgModule({
  declarations: [
    UsuariosComponent,
    EditarUsuarioComponent, RegistrarUsuarioComponent, EliminarUsuarioComponent, CambiarClaveComponent, CambiarEstadoUsuarioComponent],
  imports: [
    CommonModule,
    UsuariosRoutingModule,
    ReactiveFormsModule,
    LoadingsModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSortModule,
    MatPaginatorModule,
    ButtonsModule,
    MatButtonModule,
    BuscadorInputModule,
    ModalsModule,
    SharedFormsModule,
    UsuariosServicesModule
  ],
  entryComponents:[
    EditarUsuarioComponent, RegistrarUsuarioComponent, EliminarUsuarioComponent, CambiarClaveComponent, CambiarEstadoUsuarioComponent
  ],
  providers: [{ provide: MatPaginatorIntl, useClass: MatPaginatorIntlCro}],
})
export class UsuariosModule { }
