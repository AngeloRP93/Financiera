import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { NotificacionService } from '../../../../../services/notificacion.service';
import { UsuarioService } from '../../../../../services/usuarios-services/usuario.service';
import { Form } from '../../../../../componentes-compartidos/forms/form/form.clase';
import { Fieldset } from '../../../../../componentes-compartidos/forms/form-content/fieldset/fieldset.clase';
import { FieldsetContent } from 'src/app/componentes-compartidos/forms/form-content/fieldset/fieldset.interface';
import { Usuario } from '../../../../../usuario.interface';
import { TypeSelect } from '../../../../../componentes-compartidos/forms/form-content/selects/enum-type-select';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['../popup-usuario.scss']
})
export class EditarUsuarioComponent implements OnInit {
  formulario: Form;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Usuario,
    private notificacion: NotificacionService,
    private userService: UsuarioService
  ) {
    this.formulario = new Form(
      'editar_usuario',
      [
        new Fieldset(
          [
            new FieldsetContent(
              true,
              'email',
              data.email,
              'email',
              { hasLabel: true, texto: 'Cuenta de Usuario' },
              { required: 'Por favor escriba la cuenta de usuario' },
              { title: 'Escriba cuenta de usuario', apariencia: 'outline' },
              false,
              false
            ),
            new FieldsetContent(
              true,
              'nombres',
              data.nombres,
              'text',
              { hasLabel: true, texto: 'Nombres' },
              { required: 'Por favor escriba el nombre de usuario' },
              { title: '', apariencia: 'outline' }
            ),
            new FieldsetContent(
              true,
              'apellido_paterno',
              data.apellido_paterno,
              'text',
              { hasLabel: true, texto: 'Apellido Paterno' },
              { required: 'Por favor escriba el apellido paterno del usuario' },
              { title: '', apariencia: 'outline' }
            ),
            new FieldsetContent(
              true,
              'apellido_materno',
              data.apellido_materno,
              'text',
              { hasLabel: true, texto: 'Apellido Materno' },
              { required: 'Por favor escriba el apellido materno del usuario' },
              { title: '', apariencia: 'outline' }
            ),
            new FieldsetContent(
              false,
              'idrol',
              data.rol_nombre,
              'text',
              { hasLabel: true, texto: 'Rol' },
              null,
              { title: '', apariencia: 'outline' },
              false,
              false
            )
          
          ]
        )
      ]
    );
  }

  ngOnInit() {
  }

  private validacion(data_usuario: {nombres:string,apellido_paterno:string,apellido_materno:string}): boolean {
    if (
      data_usuario.nombres !== undefined && data_usuario.nombres !== null && data_usuario.nombres.trim() !== '' &&
      data_usuario.apellido_paterno !== undefined && data_usuario.apellido_paterno !== null && data_usuario.apellido_paterno.trim() !== '' &&
      data_usuario.apellido_materno !== undefined && data_usuario.apellido_materno !== null && data_usuario.apellido_materno.trim() !== ''
    ) {
      return true;
    } else {
      return false;
    }
  }

  private editar(event: {nombres:string,apellido_paterno:string,apellido_materno:string}) {
    this.data.nombres = event.nombres
    this.data.apellido_materno = event.apellido_materno
    this.data.apellido_paterno = event.apellido_paterno
    this.userService.editar(this.data).then(() => {
      this.formulario.loading = false;
    });
  }

  async requestBack(event: {nombres:string,apellido_paterno:string,apellido_materno:string}) {
      if (this.validacion(event)) {
        this.editar(event);
      } else {
        this.notificacion.camposImcompletos();
      }

  }

}
