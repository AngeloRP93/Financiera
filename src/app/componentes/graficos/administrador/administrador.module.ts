import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministradorRoutingModule } from './administrador-routing.module';
import { LayoutPrincipalModule } from '../../../componentes-compartidos/layout-principal/layout-principal.module';
import { LayoutAdministradorComponent } from './layout-administrador/layout-administrador.component';

@NgModule({
  declarations: [LayoutAdministradorComponent],
  imports: [
    CommonModule,
    AdministradorRoutingModule,
    LayoutPrincipalModule
  ]
})
export class AdministradorModule { }
