import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutGerenteComponent } from './layout-gerente/layout-gerente.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutGerenteComponent,
    children: [
      {
        path: '',
        redirectTo: 'graficos'
      },
      {
        path: 'graficos',
        loadChildren: 'src/app/componentes/graficos/principal/principal.module#PrincipalModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GerenteRoutingModule { }
