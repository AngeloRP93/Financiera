import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GerenteRoutingModule } from './gerente-routing.module';
import { LayoutGerenteComponent } from './layout-gerente/layout-gerente.component';
import { LayoutPrincipalModule } from '../../../componentes-compartidos/layout-principal/layout-principal.module';

@NgModule({
  declarations: [LayoutGerenteComponent],
  imports: [
    CommonModule,
    GerenteRoutingModule,
    LayoutPrincipalModule
  ]
})
export class GerenteModule { }
