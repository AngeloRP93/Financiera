import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutGerenteComponent } from './layout-gerente.component';

describe('LayoutGerenteComponent', () => {
  let component: LayoutGerenteComponent;
  let fixture: ComponentFixture<LayoutGerenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutGerenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutGerenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
