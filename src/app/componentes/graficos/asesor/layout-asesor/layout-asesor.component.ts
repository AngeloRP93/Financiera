import { Component, OnInit } from '@angular/core';
import { Layout } from '../../../../componentes-compartidos/layout-principal/layout/layout';

@Component({
  selector: 'app-layout-asesor',
  templateUrl: './layout-asesor.component.html',
  styleUrls: ['./layout-asesor.component.scss']
})
export class LayoutAsesorComponent extends Layout implements OnInit {

  constructor() {
    super('');
    this.data = {
      barra_navegacion: null,
      toolbar: { buttonIzquierdo: null, titulo: this.titulo_temp, logout: { icono: 'exit_to_app', mostrar: true }, color: 'primary' }
    }
  }

  ngOnInit() {
  }

}
