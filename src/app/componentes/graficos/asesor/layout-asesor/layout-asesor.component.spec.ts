import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutAsesorComponent } from './layout-asesor.component';

describe('LayoutAsesorComponent', () => {
  let component: LayoutAsesorComponent;
  let fixture: ComponentFixture<LayoutAsesorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutAsesorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutAsesorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
