import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutAsesorComponent } from './layout-asesor/layout-asesor.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutAsesorComponent,
    children: [
      {
        path: '',
        redirectTo: 'graficos'
      },
      {
        path: 'graficos',
        loadChildren: 'src/app/componentes/graficos/principal/principal.module#PrincipalModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsesorRoutingModule { }
