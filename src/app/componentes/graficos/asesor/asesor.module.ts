import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsesorRoutingModule } from './asesor-routing.module';
import { LayoutAsesorComponent } from './layout-asesor/layout-asesor.component';
import { LayoutPrincipalModule } from '../../../componentes-compartidos/layout-principal/layout-principal.module';

@NgModule({
  declarations: [LayoutAsesorComponent],
  imports: [
    CommonModule,
    AsesorRoutingModule,
    LayoutPrincipalModule
  ]
})
export class AsesorModule { }
