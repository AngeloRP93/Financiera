import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutSupervisorComponent } from './layout-supervisor/layout-supervisor.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutSupervisorComponent,
    children: [
      {
        path: '',
        redirectTo: 'graficos'
      },
      {
        path: 'graficos',
        loadChildren: 'src/app/componentes/graficos/principal/principal.module#PrincipalModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupervisorRoutingModule { }
