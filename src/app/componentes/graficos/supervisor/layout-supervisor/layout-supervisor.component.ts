import { Component, OnInit } from '@angular/core';
import { Layout } from '../../../../componentes-compartidos/layout-principal/layout/layout';

@Component({
  selector: 'app-layout-supervisor',
  templateUrl: './layout-supervisor.component.html',
  styleUrls: ['./layout-supervisor.component.scss']
})
export class LayoutSupervisorComponent extends Layout implements OnInit {

  constructor() {
    super('');
    this.data = {
      barra_navegacion: null,
      toolbar: { buttonIzquierdo: null, titulo: this.titulo_temp, logout: { icono: 'exit_to_app', mostrar: true }, color: 'primary' }
    }
  }

  ngOnInit() {
  }

}
