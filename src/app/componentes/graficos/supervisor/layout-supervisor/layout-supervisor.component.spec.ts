import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutSupervisorComponent } from './layout-supervisor.component';

describe('LayoutSupervisorComponent', () => {
  let component: LayoutSupervisorComponent;
  let fixture: ComponentFixture<LayoutSupervisorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutSupervisorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutSupervisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
