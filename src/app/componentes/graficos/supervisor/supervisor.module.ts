import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SupervisorRoutingModule } from './supervisor-routing.module';
import { LayoutPrincipalModule } from '../../../componentes-compartidos/layout-principal/layout-principal.module';
import { LayoutSupervisorComponent } from './layout-supervisor/layout-supervisor.component';

@NgModule({
  declarations: [LayoutSupervisorComponent],
  imports: [
    CommonModule,
    SupervisorRoutingModule,
    LayoutPrincipalModule
  ]
})
export class SupervisorModule { }
