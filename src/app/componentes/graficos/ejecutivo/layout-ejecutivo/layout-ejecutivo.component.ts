import { Component, OnInit } from '@angular/core';
import { Layout } from '../../../../componentes-compartidos/layout-principal/layout/layout';

@Component({
  selector: 'app-layout-ejecutivo',
  templateUrl: './layout-ejecutivo.component.html',
  styleUrls: ['./layout-ejecutivo.component.scss']
})
export class LayoutEjecutivoComponent extends Layout implements OnInit {

  constructor() {
    super('');
    this.data = {
      barra_navegacion: null,
      toolbar: { buttonIzquierdo: null, titulo: this.titulo_temp, logout: { icono: 'exit_to_app', mostrar: true }, color: 'primary' }
    }
  }

  ngOnInit() {
  }

}
