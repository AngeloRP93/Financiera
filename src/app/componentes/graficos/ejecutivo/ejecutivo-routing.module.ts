import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutEjecutivoComponent } from './layout-ejecutivo/layout-ejecutivo.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutEjecutivoComponent,
    children: [
      {
        path: '',
        redirectTo: 'graficos'
      },
      {
        path: 'graficos',
        loadChildren: 'src/app/componentes/graficos/principal/principal.module#PrincipalModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EjecutivoRoutingModule { }
