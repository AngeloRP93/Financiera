import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EjecutivoRoutingModule } from './ejecutivo-routing.module';
import { LayoutEjecutivoComponent } from './layout-ejecutivo/layout-ejecutivo.component';
import { LayoutPrincipalModule } from '../../../componentes-compartidos/layout-principal/layout-principal.module';

@NgModule({
  declarations: [LayoutEjecutivoComponent],
  imports: [
    CommonModule,
    EjecutivoRoutingModule,
    LayoutPrincipalModule
  ]
})
export class EjecutivoModule { }
