import { EstadosPaso } from './enums/estados.enum';

export interface EstadoMensaje {
    estado: EstadosPaso;
    mensaje: string;
}
