import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisableControlDirective } from './disable-control.directive';
import { AutotabDirective } from './autotab.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    DisableControlDirective,
    AutotabDirective
  ],
  exports: [
    DisableControlDirective,
    AutotabDirective
  ]
})
export class InputDirectivesModule { }
