import { Directive, Input, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appAutotab]'
})
export class AutotabDirective {
  @Input() tamanio: string;

  constructor(private el: ElementRef, ) { }

  @HostListener('keyup', ['$event']) actualizarInput(event) {
    
  }

}
