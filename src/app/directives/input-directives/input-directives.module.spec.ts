import { InputDirectivesModule } from './input-directives.module';

describe('InputDirectivesModule', () => {
  let inputDirectivesModule: InputDirectivesModule;

  beforeEach(() => {
    inputDirectivesModule = new InputDirectivesModule();
  });

  it('should create an instance', () => {
    expect(inputDirectivesModule).toBeTruthy();
  });
});
