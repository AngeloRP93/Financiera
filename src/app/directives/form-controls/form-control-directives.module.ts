import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisableControlDirective } from './disablecontrol.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [DisableControlDirective],
  exports: [
    DisableControlDirective
  ]
})
export class FormControlDirectivesModule { }
