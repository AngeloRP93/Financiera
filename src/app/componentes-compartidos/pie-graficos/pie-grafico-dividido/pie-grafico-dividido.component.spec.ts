import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PieGraficoDivididoComponent } from './pie-grafico-dividido.component';

describe('PieGraficoDivididoComponent', () => {
  let component: PieGraficoDivididoComponent;
  let fixture: ComponentFixture<PieGraficoDivididoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PieGraficoDivididoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PieGraficoDivididoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
