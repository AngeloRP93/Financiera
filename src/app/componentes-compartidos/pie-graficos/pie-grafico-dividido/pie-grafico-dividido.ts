
import { Input } from '@angular/core';
import { PieGrafico } from '../pie-grafico/pie-grafico';
import { PieGraficoDivididoInterface } from './pieg-grafico-dividido.interface';

export class PieGraficoDividido extends PieGrafico {
    @Input() valor_inferior: 0;
    @Input() data: PieGraficoDivididoInterface = {
        data_size: 1,
        data_pie_size: 1,
        id: '1',
        fecha: new Date(),
        font_size: 12,
        tipo: '',
        separador: '----------------',
        texto_derecho: 'Participación',
        texto_inferior: 'Plazo (días)',
        caracter_inferior: ''
    }

    public fillPieGrafico(data: PieGraficoDivididoInterface) {
        this.data = data;
    }
}