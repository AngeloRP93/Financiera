import { PieGraficoInterface } from '../pie-grafico/pie-grafico.interface';

export interface PieGraficoDivididoInterface extends PieGraficoInterface {
    texto_derecho: string;
    separador: string;
    texto_inferior: string;
    caracter_inferior: string;
}