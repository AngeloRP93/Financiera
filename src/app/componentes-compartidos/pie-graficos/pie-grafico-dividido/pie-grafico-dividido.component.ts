import { Component, OnInit } from '@angular/core';
import { PieGraficoDividido } from './pie-grafico-dividido';

@Component({
  selector: 'app-pie-grafico-dividido',
  templateUrl: './pie-grafico-dividido.component.html',
  styleUrls: ['./pie-grafico-dividido.component.scss', '../pie-grafico/pie-grafico.component.scss']
})
export class PieGraficoDivididoComponent extends PieGraficoDividido implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
    this.calcularTamanioGrafico();
    
    this.loading = false;
  }

}
