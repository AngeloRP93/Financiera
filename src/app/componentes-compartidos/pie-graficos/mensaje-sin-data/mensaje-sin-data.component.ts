import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-mensaje-sin-data',
  templateUrl: './mensaje-sin-data.component.html',
  styleUrls: ['./mensaje-sin-data.component.scss']
})
export class MensajeSinDataComponent implements OnInit {
  @Input() titulo;
  @Input() mensaje = 'Sin data para esta tienda en el rango de fecha seleccionado'
  constructor() { }

  ngOnInit() {
  }

}
