import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajeSinDataComponent } from './mensaje-sin-data.component';

describe('MensajeSinDataComponent', () => {
  let component: MensajeSinDataComponent;
  let fixture: ComponentFixture<MensajeSinDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensajeSinDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajeSinDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
