import { Component, OnInit } from '@angular/core';
import { PieGraficoDividido } from '../pie-grafico-dividido/pie-grafico-dividido';

@Component({
  selector: 'app-pie-grafico-dividido-total-arriba',
  templateUrl: './pie-grafico-dividido-total-arriba.component.html',
  styleUrls: [
    './pie-grafico-dividido-total-arriba.component.scss',
    '../pie-grafico-dividido/pie-grafico-dividido.component.scss',
    '../pie-grafico/pie-grafico.component.scss'
  ]
})
export class PieGraficoDivididoTotalArribaComponent extends PieGraficoDividido implements OnInit {
  constructor() {
    super()
  }

  ngOnInit() {
    this.calcularTamanioGrafico();
    this.loading = false;
  }

  calcularPromedio(): number {
    return (this.porcentaje + this.valor_inferior) / 2
  }

}
