import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PieGraficoDivididoTotalArribaComponent } from './pie-grafico-dividido-total-arriba.component';

describe('PieGraficoDivididoTotalArribaComponent', () => {
  let component: PieGraficoDivididoTotalArribaComponent;
  let fixture: ComponentFixture<PieGraficoDivididoTotalArribaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PieGraficoDivididoTotalArribaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PieGraficoDivididoTotalArribaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
