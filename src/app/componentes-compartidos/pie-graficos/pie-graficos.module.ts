import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PieGraficoComponent } from './pie-grafico/pie-grafico.component';
import { PieGraficoDivididoComponent } from './pie-grafico-dividido/pie-grafico-dividido.component';
import { MatCardModule, MatProgressSpinnerModule, MatDividerModule } from '@angular/material';
import { PieGraficoDivididoTotalArribaComponent } from './pie-grafico-dividido-total-arriba/pie-grafico-dividido-total-arriba.component';
import { MensajeSinDataComponent } from './mensaje-sin-data/mensaje-sin-data.component';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatDividerModule
  ],
  declarations: [PieGraficoComponent, PieGraficoDivididoComponent, PieGraficoDivididoTotalArribaComponent, MensajeSinDataComponent],
  exports: [PieGraficoComponent, PieGraficoDivididoComponent,PieGraficoDivididoTotalArribaComponent, MensajeSinDataComponent]
})
export class PieGraficosModule { }
