export interface PieGraficoInterface {
    data_size: number;
    data_pie_size: number;
    font_size: number;
    fecha: any,
    id: string;
    tipo: string;
}
