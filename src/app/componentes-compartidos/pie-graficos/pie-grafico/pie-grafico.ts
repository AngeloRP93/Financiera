import { PieGraficoInterface } from './pie-grafico.interface';
import { Input, EventEmitter, Output, HostListener } from '@angular/core';
import { ThemePalette } from '@angular/material';
export class PieGrafico {
    mostrarGraficos: boolean;
    @Input() mode = 'determinate';
    color: ThemePalette;
    value = 50;
    loading = true;
    buttonStyles: any;
    @Input() porcentaje = 50;
    @Input('data_color')
    set data_color(texto: string) {
        switch (texto) {
            case 'oeschle':
                this._data_color = 'warn'
                break;
            case 'crediventas':
                this._data_color = 'accent'
                break;
            default:
                this._data_color = 'primary'
                break;
        }
    };
    get data_color(): string {
        return this._data_color
    }
    @Input() titulo = 'Example';
    private _data_color: string;
    stroke = 15;
    data: PieGraficoInterface = {
        data_size: 1,
        data_pie_size: 1,
        id: '1',
        fecha: new Date(),
        font_size: 12,
        tipo: ''
    };
    @Output() regresar: EventEmitter<any>;
    fecha: any;
    mostrarGrafica = 'noMostrar';
    constructor() {
        this.mostrarGraficos = true;
        this.regresar = new EventEmitter<any>();
    }

    capturarEvento(event) {
        if (event === true) {
            this.mostrarGrafica = 'mostrar';
        }
    }

    public fillPieGrafico(data: PieGraficoInterface) {
        this.data = data;
    }

    public calcularTamanioGrafico() {
        const width = window.innerWidth;
        if (width >= 1200) {
            this.data.data_size = (width - 300) / 6;
        } else if (width > 992) {
            this.data.data_size = (width - 140) / 3;
        } else {
            this.data.data_size = (width - 140) / 2;
        }
        this.data.data_pie_size = Math.round(this.data.data_size);
        this.data.font_size = this.data.data_pie_size / 5;
        this.loading = false;
    }

    public darFormatoFecha() {
        const array = this.data.fecha.split('-');
        this.fecha = array[0] + '/' + array[1] + '/' + array[2];
    }

    public salirGrafico() {
        this.regresar.emit(
            this.porcentaje
        );
    }

    render() { }
}