import { Component, OnInit } from '@angular/core';
import { PieGrafico } from './pie-grafico';

@Component({
  selector: 'app-pie-grafico',
  templateUrl: './pie-grafico.component.html',
  styleUrls: ['./pie-grafico.component.scss']
})
export class PieGraficoComponent extends PieGrafico implements OnInit {
  
  constructor() {
    super();
  }

  ngOnInit() {
    this.calcularTamanioGrafico();
  }

}
