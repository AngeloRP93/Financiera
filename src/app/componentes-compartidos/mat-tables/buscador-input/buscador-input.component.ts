import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-buscador-input',
  templateUrl: './buscador-input.component.html',
  styleUrls: ['./buscador-input.component.scss']
})
export class BuscadorInputComponent implements OnInit {
  @Output() buscar: EventEmitter<string> = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }

}
