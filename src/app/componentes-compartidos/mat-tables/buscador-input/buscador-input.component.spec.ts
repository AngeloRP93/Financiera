import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscadorInputComponent } from './buscador-input.component';

describe('BuscadorInputComponent', () => {
  let component: BuscadorInputComponent;
  let fixture: ComponentFixture<BuscadorInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuscadorInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscadorInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
