import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuscadorInputComponent } from './buscador-input.component';
import { MatIconModule, MatInputModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule
  ],
  declarations: [BuscadorInputComponent],
  exports:[BuscadorInputComponent]
})
export class BuscadorInputModule { }
