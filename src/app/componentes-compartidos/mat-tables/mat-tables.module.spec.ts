import { MatTablesModule } from './mat-tables.module';

describe('MatTablesModule', () => {
  let matTablesModule: MatTablesModule;

  beforeEach(() => {
    matTablesModule = new MatTablesModule();
  });

  it('should create an instance', () => {
    expect(matTablesModule).toBeTruthy();
  });
});
