import { Input, Output, EventEmitter } from '@angular/core';
import { FormControl, ValidatorFn } from '@angular/forms';
export class FormControlClass {
    @Input() contenido: any;
    @Output() cambio: EventEmitter<any>;
    formControl: FormControl;
    validators: ValidatorFn[];
    activo = true;

    constructor() {
        this.cambio = new EventEmitter<any>();
    }

    protected fillValidators() {

    }

    protected fillContenido() {
        setTimeout(() => {
            this.formControl.setValue(this.contenido.data.value, { emitEvent: false });
        }, 1);
    }

    getErrorMessage() {        
        return this.formControl.hasError('required') ? this.contenido.advertencia.required :
            this.formControl.hasError('pattern') ? this.contenido.advertencia.pattern :
                this.formControl.hasError('min') ? this.contenido.advertencia.min :
                    this.formControl.hasError('max') ? this.contenido.advertencia.max :
                        '';
    }
}