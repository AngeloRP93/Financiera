import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading-onda',
  templateUrl: './loading-onda.component.html',
  styleUrls: [
    './loading-onda.component.css',
    '../container.load.scss'
  ]
})
export class LoadingOndaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
