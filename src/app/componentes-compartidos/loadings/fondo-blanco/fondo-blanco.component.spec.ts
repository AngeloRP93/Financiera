import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FondoBlancoComponent } from './fondo-blanco.component';

describe('FondoBlancoComponent', () => {
  let component: FondoBlancoComponent;
  let fixture: ComponentFixture<FondoBlancoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FondoBlancoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FondoBlancoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
