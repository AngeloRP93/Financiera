import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingOndaComponent } from './loading-onda/loading-onda.component';
import { FondoBlancoComponent } from './fondo-blanco/fondo-blanco.component';
import { MatProgressSpinnerModule } from '@angular/material';
import { SpinnerComponent } from './spinner/spinner.component';

@NgModule({
  imports: [
    CommonModule,
    MatProgressSpinnerModule
  ],
  declarations: [LoadingOndaComponent, FondoBlancoComponent, SpinnerComponent],
  exports: [
    LoadingOndaComponent,
    FondoBlancoComponent,
    SpinnerComponent
  ]
})
export class LoadingsModule { }
