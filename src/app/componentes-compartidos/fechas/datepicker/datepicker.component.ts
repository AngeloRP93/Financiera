import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { FormControl, Validators } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { FormControlClass } from '../../form-control';
import { takeWhile } from 'rxjs/operators';
import * as _moment from 'moment';
export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class DatepickerComponent extends FormControlClass implements OnInit, OnDestroy {
  @Input() minDate = new Date();
  @Input() maxDate = new Date();
  constructor() {
    super();
    this.actualizarValor();
    // this.formControl = new FormControl('', [Validators.required, Validators.pattern(this.datePattern)]);
  }

  /*formatoFecha(fecha: string) {
    const indice = fecha.lastIndexOf('-') + 1;
    const ultimo = fecha.substring(indice, indice + 2);
    return fecha.substring(0, indice) + ultimo;
  }*/

  ngOnInit() {
    this.fillValidators();
  }

  protected fillValidators() {
    this.validators = [];
    this.validators.push(Validators.required);
    this.formControl = new FormControl('',this.validators);
    this.fillContenido();
  }

  private actualizarValor() {
    setTimeout(() => {
      this.formControl.valueChanges.pipe(takeWhile(() => this.activo)).subscribe(
        (value) => {
         const fecha = this.devolverFecha(value);
          if (value !== null) {
            this.cambio.emit(fecha);
          }
        }
      );
    }, 0);
  }


  private devolverFecha(value: string): string {
    let fecha;
    if (value) {
      if (_moment.isMoment(value)) {
        
        fecha = _moment(value).format('YYYYMMDD');
      } else {
        if (value.includes('/')) {
          fecha = value;
        } else {
          return value;
        }
      }
      return fecha;
    } else {
      return null;
    }
  }

  ngOnDestroy() {
    this.activo = false;
  }

}