import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDatepickerModule, MatIconModule, MatInputModule, MatButtonModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { FormControlDirectivesModule } from '../../directives/form-controls/form-control-directives.module';

@NgModule({
  imports: [
    CommonModule,
    MatDatepickerModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatMomentDateModule,
    FormControlDirectivesModule
  ],
  declarations: [
    DatepickerComponent],
  exports: [
    DatepickerComponent]
})
export class FechasModule { }
