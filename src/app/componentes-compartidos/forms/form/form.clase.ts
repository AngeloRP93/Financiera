import { Input, Output, EventEmitter } from '@angular/core';
import { FormInterface } from './form.interface';
import { isBoolean, isString } from 'util';
import { ContenedorFilas } from '../contenedorFilas';
import { Fieldset } from '../form-content/fieldset/fieldset.clase';
import { isMoment } from 'moment';

export class Form extends ContenedorFilas {
  requestJson: any = {};
  responseJson: any = {};
  @Input() data: FormInterface = null;
  loading: boolean;
  fieldsetAgregado: boolean;
  @Input() loading_id = '';
  @Input() formaParteDeTabla = false;
  @Output() validarFormulario = new EventEmitter<any>();
  constructor(
    id: string = '',
    fieldsets: Fieldset[] = new Array<Fieldset>(),
    hasFooter: boolean = true,
    clase: string = '') {
    super();
    this.data = {
      id: id,
      clase: clase,
      hasFooter: hasFooter,
      fieldsets: fieldsets
    };
    this.loading = false;
    this.fieldsetAgregado = false;
  }

  editarFormulario(
    id: string,
    clase: string,
    fieldsets: Fieldset[],
    hasFooter: boolean = true,
  ) {
    this.loading = false;
    this.data = {
      fieldsets: fieldsets,
      clase: clase,
      id: id,
      hasFooter: hasFooter
    };
  }

  activarFieldsets() {
    for (let index = 0; index < this.data.fieldsets.length; index++) {
      const fieldset = this.data.fieldsets[index];
      for (let index2 = 0; index2 < fieldset.data.inputs.length; index2++) {
        const input = fieldset.data.inputs[index2].contenido;
        if (input.data !== undefined) {
          input.data.isRequired = true;
          input.data.isSeteable = true;
        }
      }
    }
  }

  agregarFieldset(nuevoFieldset: Fieldset, indice: number = -1) {
    if (indice < 0) {
      this.data.fieldsets.push(nuevoFieldset);
    } else {
      if (indice === 0) {
        this.data.fieldsets.unshift(nuevoFieldset);
      } else {
        const primeraParte = this.data.fieldsets.slice(0, indice);
        const segundaParte = this.data.fieldsets.slice(indice, this.data.fieldsets.length);
        primeraParte.push(nuevoFieldset);
        this.data.fieldsets = primeraParte.concat(segundaParte);
      }
    }
  }

  eliminarUltimoFieldset() {
    this.data.fieldsets.pop();
  }

  eliminarSiAgregado() {
    if (this.fieldsetAgregado === true) {
      this.eliminarUltimoFieldset();
      this.fieldsetAgregado = false;
    }
  }

  agregarSoloUnFieldset(nuevoFieldset: Fieldset) {
    this.eliminarSiAgregado();
    this.agregarFieldset(nuevoFieldset);
    this.fieldsetAgregado = true;
  }

  submitForm(event) {
    if (!isBoolean(event)) {
      event.preventDefault();
    }
    this.generarResponse();
  }

  protected generarResponse() {
    for (let index = 0; index < this.data.fieldsets.length; index++) {
      this.generarResponseFieldset(index);
    }
    this.requestBack();
  }

  protected emitirValueChanged(data, indiceFieldset: any) {
    this.valueChange.emit(data);
  }

  public generarResponseFieldset(indice: number) {
    const fieldset = this.data.fieldsets[indice];
    for (let index2 = 0; index2 < fieldset.data.inputs.length; index2++) {
      const input = fieldset.data.inputs[index2];
      if (input.contenido.data) {
        if (input.contenido.data.isRequired === true) {
          if (isMoment(input.contenido.data.value)) {
            this.requestJson[input.contenido.data.id] =
              input.contenido.data.value.year() + '-' +
              this.completarConCero(input.contenido.data.value.month() + 1) + '-' +
              this.completarConCero(input.contenido.data.value.date());
          } else {
            this.requestJson[input.contenido.data.id] = input.contenido.data.value;
          }
        }
      }
    }
  }

  requestBack(indice = 0) {
    this.validarFormulario.emit(this.requestJson);
  }

  eliminarFieldSet(indiceFieldset: number): boolean {
    this.data.fieldsets.splice(indiceFieldset, 1);
    return true;
  }

  private completarConCero(numero: number): string {
    if (numero < 10) {
      return '0' + numero;
    } else {
      return '' + numero;
    }
  }
}
