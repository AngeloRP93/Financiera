import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Form } from './form.clase';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends Form implements OnInit, OnChanges {
  constructor() {
    super();
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.isPressed && !changes.isPressed.isFirstChange()) {
      setTimeout(() => {
        this.submitForm(true);
      }, 0);
    }

  }

}
