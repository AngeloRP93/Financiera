import { Input } from '@angular/core';
export class Presionable {
    @Input() isPressed = false;
}