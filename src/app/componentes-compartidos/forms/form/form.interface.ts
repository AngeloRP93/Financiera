import { Fieldset } from '../form-content/fieldset/fieldset.clase';
export interface FormInterface {
  fieldsets: Fieldset[];
  clase: string ;
  id: string ;
  hasFooter: boolean ;
}
