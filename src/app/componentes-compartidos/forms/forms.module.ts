import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormComponent } from './form/form.component';
import { FormContentModule } from './form-content/form-content.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FormContentModule,
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [
    FormComponent
  ],
  exports: [
    FormComponent,
    FormContentModule
  ]
})
export class SharedFormsModule { }
