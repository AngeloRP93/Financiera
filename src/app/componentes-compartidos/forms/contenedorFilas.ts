import { Presionable } from './form/presionable';
import { Output, EventEmitter } from '@angular/core';

export class ContenedorFilas extends Presionable {
    @Output() valueChange = new EventEmitter<any>();

    protected emitirValueChanged(data: any, indice: any) {
        this.valueChange.emit(data);
    }
}
