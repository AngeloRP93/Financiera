import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Select } from '../select';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent extends Select implements OnInit, OnDestroy {

  constructor(
    private changeDetector: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit() {
    this.fillValidators();
  }

  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }

  ngOnDestroy() {
    this.activo = false;
  }

  emitirValueChanged(data: any) {
  }

  selectChange(event: number) {
    const local = this.contenido.data.options.filter(
      (item) => {
        return item.id === event;
      }
    );
    this.changeValue.emit(
      local[0]
    );
  }
}
