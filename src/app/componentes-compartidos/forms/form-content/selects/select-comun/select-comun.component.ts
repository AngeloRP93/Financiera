import { Component, OnInit, ChangeDetectorRef, AfterViewChecked, OnDestroy } from '@angular/core';
import { Select } from '../select';

@Component({
  selector: 'app-select-comun',
  templateUrl: './select-comun.component.html',
  styleUrls: ['./select-comun.component.css']
})
export class SelectComunComponent extends Select implements OnInit, AfterViewChecked, OnDestroy {

  constructor(
    private changeDetector: ChangeDetectorRef
  ) {
    super();
  }

  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }

  ngOnInit() {
    this.fillValidators();
  }

  ngOnDestroy() {
    this.activo = false;
  }

  selectChange(event: number) {
    const local = this.contenido.data.options.filter(
      (item) => {
        return item.id === event;
      }
    );
    this.changeValue.emit(
      local[0]
    );
  }

}
