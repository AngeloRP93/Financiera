import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectSearch } from '../select-search/select-search';

@Component({
  selector: 'app-select-search-multiple',
  templateUrl: './select-search-multiple.component.html',
  styleUrls: ['./select-search-multiple.component.scss', '../select-search/select-search.component.scss']
})
export class SelectSearchMultipleComponent extends SelectSearch implements OnInit, OnDestroy {
  
  constructor() {
    super();
  }

  ngOnInit() {
    this.itemsTemp = this.contenido.data.options;
    this.fillValidators();
  }

  ngOnDestroy() {
    this.activo = false;
  }

}
