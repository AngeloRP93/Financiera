import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { TypeSelect } from '../enum-type-select';
import { Select } from '../select';

@Component({
  selector: 'app-select-content',
  templateUrl: './select-content.component.html',
  styleUrls: ['./select-content.component.scss']
})
export class SelectContentComponent extends Select implements OnInit, OnDestroy {
  // @Input() type: TypeSelect = TypeSelect.select;
  public enum = TypeSelect;
  constructor() {
    super();
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.activo = false;
  }

}
