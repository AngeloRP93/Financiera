import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectComunComponent } from './select-comun/select-comun.component';
import { SelectMultipleComponent } from './select-multiple/select-multiple.component';
import { SelectSearchMultipleComponent } from './select-search-multiple/select-search-multiple.component';
import {MatSelectModule} from '@angular/material/select';
import { MaterialFormsModule } from '../../../material-forms/material-forms.module';
import { SelectContentComponent } from './select-content/select-content.component';
import { MatIconModule } from '@angular/material';
import { InputDirectivesModule } from '../../../../directives/input-directives/input-directives.module';
import { SelectSearchComponent } from './select-search/select-search.component';
import { SelectComponent } from './select/select.component';
import { SelectSearchFinantiendasComponent } from './select-search-finantiendas/select-search-finantiendas.component';

@NgModule({
  imports: [
    CommonModule,
    MatSelectModule,
    MaterialFormsModule,
    MatIconModule,
    InputDirectivesModule
  ],
  declarations: [
    SelectComunComponent,
    SelectMultipleComponent,
    SelectSearchMultipleComponent,
    SelectContentComponent,
    SelectSearchComponent,
    SelectSearchFinantiendasComponent,
    SelectComponent
  ],
  exports: [
    SelectComunComponent,
    SelectComponent,
    SelectMultipleComponent,
    SelectSearchMultipleComponent,
    SelectContentComponent,
    SelectSearchComponent,
    SelectSearchFinantiendasComponent
  ]
})
export class SelectsModule { }
