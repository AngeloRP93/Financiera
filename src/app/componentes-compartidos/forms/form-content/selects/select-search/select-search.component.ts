import { Component, OnInit } from '@angular/core';
import { Select } from '../select';
import { SelectSearch } from './select-search';

@Component({
  selector: 'app-select-search',
  templateUrl: './select-search.component.html',
  styleUrls: ['./select-search.component.scss']
})
export class SelectSearchComponent extends SelectSearch implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
    this.itemsTemp = this.contenido.data.options;
    this.fillValidators();
  }

  ngOnDestroy() {
    this.activo = false;
  }

}
