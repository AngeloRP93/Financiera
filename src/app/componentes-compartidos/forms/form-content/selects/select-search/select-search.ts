import { Select } from '../select';
export class SelectSearch extends Select {
    searchValue: string;
    itemsTemp: { id: any, nombre: string }[];
    valido = true;
    constructor() {
        super();
        this.searchValue = '';
    }

    selectChange(event) {
        if (event.length > 2) {
            this.valido = false;
        } else {
            this.valido = true;
        }
    }

    buscar(value: string) {
        this.contenido.data.options = this.itemsTemp.filter(
            (item) => {
                let nombre = item.nombre + '';
                return nombre.toUpperCase().includes(value.toUpperCase());
            }
        );
    }
}