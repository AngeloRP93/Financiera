import { Component, OnInit, OnDestroy } from '@angular/core';
import { Select } from '../select';

@Component({
  selector: 'app-select-multiple',
  templateUrl: './select-multiple.component.html',
  styleUrls: ['./select-multiple.component.css']
})
export class SelectMultipleComponent extends Select implements OnInit, OnDestroy {

  constructor() {
    super();
  }

  ngOnInit() {
    this.fillValidators();
  }

  ngOnDestroy() {
    this.activo = false;
  }

}
