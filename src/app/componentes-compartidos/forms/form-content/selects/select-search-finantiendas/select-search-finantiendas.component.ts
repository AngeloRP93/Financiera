import { Component, OnInit, EventEmitter, Output, OnDestroy, Input } from '@angular/core';
import { FormControl, ValidatorFn, Validators, NgForm, FormGroupDirective } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}


@Component({
  selector: 'app-select-search-finantiendas',
  templateUrl: './select-search-finantiendas.component.html',
  styleUrls: ['./select-search-finantiendas.component.scss']
})
export class SelectSearchFinantiendasComponent implements OnInit, OnDestroy {
  @Input() contenido: any;
  @Output() cambio: EventEmitter<any> = new EventEmitter<any>();
  formControl: FormControl;
  validators: ValidatorFn[];
  activo = true;
  matcher = new MyErrorStateMatcher();
  searchValue: any;
  itemsTemp: any;
  valido = false;
  constructor() {
    this.fillValidators();
    this.searchValue = '';
  }

  ngOnInit() {
    this.itemsTemp = this.contenido.data.options;
    this.fillValidators();
  }

  protected fillValidators() {
    this.validators = [];
    this.formControl = new FormControl('', Validators.required);
    // this.emitirFormControl(this.formControl);
    this.fillContenido();
  }

  protected fillContenido() {
    setTimeout(() => {
      this.formControl.setValue(this.contenido.data.value, { emitEvent: false });
    }, 1);
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? this.contenido.advertencia.required :
      this.formControl.hasError('pattern') ? this.contenido.advertencia.pattern :
        this.formControl.hasError('min') ? this.contenido.advertencia.min :
          this.formControl.hasError('max') ? this.contenido.advertencia.max :
            '';
  }

  selectChange(event: number) {
    const local = this.contenido.data.options.filter(
      (item) => {
        return item.local_key === event;
      }
    );
    this.contenido.data.label.texto = 'Finantienda';
    this.formControl.setValue(null, { emitEvent: false });
    this.cambio.emit(
      local[0]
    );
  }

  buscar(value: string) {
    this.contenido.data.options = this.itemsTemp.filter(
      (item) => {
        return item.local.toUpperCase().includes(value.toUpperCase());
      }
    );
  }

  ngOnDestroy() {
    this.activo = false;
  }

}
