import { ErrorStateMatcher } from '@angular/material';
import { FormGroupDirective, NgForm, FormControl } from '@angular/forms';
import { Input } from '@angular/core';
import { FormContent } from '../form-content';
import { FieldsetSelect } from '../fieldset/fieldset.interface';

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

export class Select extends FormContent {
    @Input() contenido: FieldsetSelect;
    matcher = new MyErrorStateMatcher();
    constructor() {
        super();
    }

}
