export enum TypeSelect {
    select = 'select',
    select_search_multiple = 'select_search_multiple',
    select_multiple = 'select_multiple',
    select_search = 'select_search'
}
