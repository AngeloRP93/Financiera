import { InputInterface } from '../inputs/input.interface';
import { InputLabelInterface } from '../inputs/input_label.interface';
export class SelectInterface extends InputInterface {
    options: {id:any,nombre:any}[];
    selectedItems: {id:any,nombre:any}[];
    dropdownSettings: any;
    constructor(
        id: string = '',
        label: InputLabelInterface = {
            hasLabel: true,
            texto: ''
        },
        value: any = null,
        type: 'text' = 'text',
        isSeteable: boolean = true,
        isRequired: boolean = true,
        clase: string = '',
        options: {id:any,nombre:any}[] = [],
        selectedItems: {id:any,nombre:any}[] = [],
        dropdownSettings: any = null
    ) {
        super(id, label, value, type, isSeteable, isRequired, clase);
        this.options = options;
        this.selectedItems = selectedItems;
        this.dropdownSettings = dropdownSettings;
    }

    public toString = (): string => {

        return `SelectInterface ( id: ${this.id}, value: ${this.value}, isRequired: ${this.isRequired},  options: ${JSON.stringify(this.options)})`;
    }
}
