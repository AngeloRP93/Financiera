import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Component, ElementRef, Input, Output, OnDestroy, OnInit, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material';
import { Subject } from 'rxjs';
import { InputClassContent } from '../input-content.class';
declare var $: any;

/** Data structure for holding telephone number. */
export class MyTel {
  constructor(
    public first: string,
    public area: string,
    public exchange: string,
    public subscriber: string
  ) { }

  capturarValue(phone: number) {
    const _phone = '' + phone;
    this.first = _phone.substring(0, 2);
    this.area = _phone.substring(2, 5);
    this.exchange = _phone.substring(5, 8);
    this.subscriber = _phone.substring(8, 11);
  }
}


/** Custom `MatFormFieldControl` for telephone number input. */
@Component({
  selector: 'app-my-tel-input',
  templateUrl: './input-phone.component.html',
  styleUrls: ['./input-phone.component.scss'],
  providers: [{ provide: MatFormFieldControl, useExisting: MyTelInputComponent }],
  host: {
    '[class.floating]': 'shouldLabelFloat',
    '[id]': 'id',
    '[attr.aria-describedby]': 'describedBy',
  }
})
export class MyTelInputComponent implements MatFormFieldControl<MyTel>, OnInit, OnDestroy {
  static nextId = 0;
  parts: FormGroup;

  stateChanges = new Subject<void>();

  focused = true;

  ngControl = null;

  errorState = false;

  controlType = 'app-my-tel-input';

  get empty() {
    const n = this.parts.value;
    return !n.first && !n.area && !n.exchange && !n.subscriber;
  }

  get validos() {
    const value = this.parts.value;
    return !isNaN(value.first) && !isNaN(value.area) && !isNaN(value.exchange) && !isNaN(value.subscriber);
  }

  get shouldLabelFloat() { return this.focused || !this.empty; }

  id = `app-my-tel-input-${MyTelInputComponent.nextId++}`;

  describedBy = '';
  @Output() valorActualizado = new EventEmitter<string>();
  @Input() myTel: MyTel;
  @Input() isSeteable: boolean;
  @Input() idInput: string;
  @Input()
  get placeholder() { return this._placeholder; }
  set placeholder(plh) {
    this._placeholder = plh;
    this.stateChanges.next();
  }
  private _placeholder: string;

  @Input()
  get required() { return this._required; }
  set required(req) {
    this._required = coerceBooleanProperty(req);
    this.stateChanges.next();
  }
  private _required = false;

  @Input()
  get disabled() { return this._disabled; }
  set disabled(dis) {
    this._disabled = coerceBooleanProperty(dis);
    this.stateChanges.next();
  }
  private _disabled = false;

  @Input()
  get value(): MyTel | null {
    const n = this.parts.value;
    if (
      n.first.length === 2 &&
      n.area.length === 3 &&
      n.exchange.length === 3 &&
      n.subscriber.length === 3) {
      return new MyTel(n.first, n.area, n.exchange, n.subscriber);
    }
    return null;
  }
  set value(tel: MyTel | null) {
    tel = tel || new MyTel('51', '', '', '');
    this.parts.setValue({ first: tel.first, area: tel.area, exchange: tel.exchange, subscriber: tel.subscriber });
    this.stateChanges.next();
  }

  ngOnInit() {
    this.parts.setValue({
      first: this.myTel.first,
      area: this.myTel.area,
      exchange: this.myTel.exchange,
      subscriber: this.myTel.subscriber
    });
  }

  constructor(fb: FormBuilder, private fm: FocusMonitor, private elRef: ElementRef) {
    this.parts = fb.group({
      'first': '51',
      'area': '',
      'exchange': '',
      'subscriber': '',
    });
    setTimeout(() => {
      this.parts.valueChanges.subscribe(
        (value) => {
          if (!this.empty && this.validos) {
            this.valorActualizado.emit(
              '' + value.first + value.area + value.exchange + value.subscriber
            );
          }
        }
      );
    }, 0);
    fm.monitor(elRef.nativeElement, true).subscribe((origin) => {
      this.focused = !!origin;
      this.stateChanges.next();
    });
  }

  onInputEntry(event, id: string, nextInputIndex: any, limite: number) {
    const input = this.parts.get(event).value;
    if (input.length === limite || input.length === 0) {
      let newID = '';
      if (input.length === 0) {
        if (nextInputIndex > 2) {
          nextInputIndex-=2;
        } else {
          if (nextInputIndex === 2) {
            nextInputIndex='';
          } else {
            return;
          }
        }
      }
      newID = id + nextInputIndex;
      let idFinal = '#phone_input #' + newID;
      if (this.idInput !== '') {
        idFinal = '#' + this.idInput + ' ' + idFinal;
      }

      $(idFinal).focus();
    }
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this.fm.stopMonitoring(this.elRef.nativeElement);
  }

  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(event: MouseEvent) {
    if ((event.target as Element).tagName.toLowerCase() !== 'input') {
      this.elRef.nativeElement.querySelector('input').focus();
    }
  }
}


/** @title Form field with custom telephone number input control. */
@Component({
  selector: 'app-input-phone',
  template: `
    <mat-form-field hideRequiredMarker="false"
    appearance="{{contenido.options.apariencia}}"
    id="{{contenido.data.id}}"
    >
      <mat-label *ngIf="contenido.data.label.hasLabel">{{contenido.data.label.texto}}</mat-label>
      <app-my-tel-input
      [myTel]="tel"
      [idInput]="fieldsetId"
      [isSeteable]="contenido.data.isSeteable"
      [required]="contenido.data.isRequired"
      (valorActualizado)="capturarPhoneNumber($event)"
      placeholder="Phone number" >
      </app-my-tel-input>
    </mat-form-field>
  `
})
export class InputPhoneComponent extends InputClassContent implements OnInit, OnDestroy {
  tel: MyTel;
  loading = true;
  constructor() {
    super();
  }

  ngOnInit() {
    this.fillValidators();
    let _phone = '';
    let contador = 1;
    let first = '51';
    let area = '';
    let exchange = '';
    let subscriber = '';
    let indice = 0;
    if (this.contenido.data.value !== null) {
      _phone += this.contenido.data.value;
      while (indice <= _phone.length) {
        switch (contador) {
          case 1:
            first = _phone.substring(indice, 2);
            indice += 2;
            break;
          case 2:
            area = _phone.substring(indice, 5);
            indice += 3;
            break;
          case 3:
            exchange = _phone.substring(indice, 8);
            indice += 3;
            break;
          case 4:
            subscriber = _phone.substring(indice, 11);
            indice += 3;
            indice = _phone.length + 1;
            break;
          default:
            indice = _phone.length + 1;
            break;
        }
        contador++;
      }
    }



    this.tel = new MyTel(
      first,
      area,
      exchange,
      subscriber
    );

  }

  capturarPhoneNumber(phone) {
    this.contenido.data.value = phone;
    this.changeValue.emit(phone);
  }

  ngOnDestroy() {
    this.activo = false;
  }
}
