import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputComponent } from './input/input.component';
import { InputContentComponent } from './input-content/input-content.component';
import { InputHiddenComponent } from './input-hidden/input-hidden.component';
import { InputFileComponent } from './input-file/input-file.component';
import { InputCheckboxComponent } from './input-checkbox/input-checkbox.component';
import { InputPhoneComponent, MyTelInputComponent } from './input-phone/input-phone.component';
import { InputMillarComponent } from './input-millar/input-millar.component';
import { InputDateComponent } from './input-date/input-date.component';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IconoModule } from '../icono/icono.module';
// import { MatInputModule, MomentDateModule, MatIconModule, MatChipsModule } from '@angular/material';
// import { MatFormFieldModule } from '@angular/material/form-field';
import { SelectsModule } from '../selects/selects.module';
import { MatDatepickerModule, MatIconModule, MatChipsModule } from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { InputDirectivesModule } from '../../../../directives/input-directives/input-directives.module';
import { InputUrlComponent } from './input-url/input-url.component';
import { TextAreaComponent } from './text-area/text-area.component';
import { DecimalMaskDirective } from './decimal.directive';
import { InputMonthComponent } from './input-month/input-month.component';
import { RouterModule } from '@angular/router';
import { InputHoraComponent } from './input-hora/input-hora.component';
import { InputChipsComponent } from './input-chips/input-chips.component';
import { MaterialFormsModule } from '../../../material-forms/material-forms.module';



@NgModule({
  imports: [
    CommonModule,
    IconoModule,
    InputDirectivesModule,
    MatIconModule,
    MaterialFormsModule,
    MatChipsModule,
    SelectsModule,
    MatDatepickerModule,
    MatMomentDateModule,
    RouterModule
  ],
  declarations: [
    InputComponent,
    InputContentComponent,
    InputHiddenComponent,
    InputFileComponent,
    InputCheckboxComponent,
    InputPhoneComponent,
    InputMillarComponent,
    InputDateComponent,
    MyTelInputComponent,
    InputUrlComponent,
    TextAreaComponent,
    DecimalMaskDirective,
    InputMonthComponent,
    InputHoraComponent,
    InputChipsComponent
  ],
  exports: [
    InputComponent,
    InputContentComponent,
    InputHiddenComponent,
    InputFileComponent,
    InputCheckboxComponent,
    InputPhoneComponent,
    InputMillarComponent,
    InputDateComponent,
    MyTelInputComponent,
    InputUrlComponent,
    TextAreaComponent,
    DecimalMaskDirective,
    InputMonthComponent,
    InputHoraComponent,
    InputChipsComponent
  ]
})
export class InputsModule { }
