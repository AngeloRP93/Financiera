import { Component, OnInit } from '@angular/core';
import { FormContent } from '../../form-content';

@Component({
  selector: 'app-input-file',
  templateUrl: './input-file.component.html',
  styleUrls: ['./input-file.component.css']
})
export class InputFileComponent  extends FormContent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
