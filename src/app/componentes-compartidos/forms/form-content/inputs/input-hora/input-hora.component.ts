import { Component, OnInit, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import { InputClassContent } from '../input-content.class';
import * as _moment from 'moment';
const moment = _moment;
@Component({
  selector: 'app-input-hora',
  templateUrl: './input-hora.component.html',
  styleUrls: ['./input-hora.component.scss']
})
export class InputHoraComponent extends InputClassContent implements OnInit, AfterViewChecked {
  loading = true;
  constructor(
    private changeDetector: ChangeDetectorRef
  ) {
    super()
    // this.cdr.detectChanges();
  }

  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }

  ngOnInit() {
    this.fillValidators();
    this.loading = false;

  }

}
