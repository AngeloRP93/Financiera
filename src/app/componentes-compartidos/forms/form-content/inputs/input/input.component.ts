import { Component, OnInit } from '@angular/core';
import { FormContent } from '../../form-content';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent extends FormContent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
