import { Component, OnInit, OnDestroy } from '@angular/core';
import { InputClassContent } from '../input-content.class';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-input-url',
  templateUrl: './input-url.component.html',
  styleUrls: ['./input-url.component.scss']
})
export class InputUrlComponent extends InputClassContent implements OnInit, OnDestroy {
  constructor() {
    super();
  }

  ngOnInit() {
    if ((this.contenido.data.value + '').includes('https://')) {
    } else {
      this.contenido.options.pattern = '[a-zA-Z]{1,1}[a-zA-Z0-9_*@¿?!¡+]+[.]{1,1}[a-zA-Z]{2,10}';
    }
    this.fillValidators();
  }

  ngOnDestroy() {
    this.activo = false;
  }

  actualizarValor() {
    setTimeout(() => {
      this.contenido.formControl.valueChanges.pipe(takeWhile(() => this.activo)).subscribe(
        (value) => {
          if (value !== null) {
            const id = value.lastIndexOf('https');
            if (id === -1) {
              this.contenido.data.value = 'https://' + value;
            } else {
              this.contenido.data.value = value;
            }
            this.emitirValueChanged(this.contenido.data);
          }
        }
      );
    }, 0);
  }

}
