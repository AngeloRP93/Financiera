import { Component, OnInit, OnDestroy } from '@angular/core';
import { InputClassContent } from '../input-content.class';

@Component({
  selector: 'app-input-millar',
  templateUrl: './input-millar.component.html',
  styleUrls: ['./input-millar.component.css']
})
export class InputMillarComponent extends InputClassContent implements OnInit, OnDestroy {

  constructor() {
    super();
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.activo = false;
  }


}
