import { Component, OnInit } from '@angular/core';
import { FormContent } from '../../form-content';

@Component({
  selector: 'app-input-hidden',
  templateUrl: './input-hidden.component.html',
  styleUrls: ['./input-hidden.component.css']
})
export class InputHiddenComponent  extends FormContent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
