import { Component, OnInit, ChangeDetectorRef, AfterViewChecked, OnDestroy } from '@angular/core';
import { InputClassContent } from '../input-content.class';
@Component({
  selector: 'app-input-content',
  templateUrl: './input-content.component.html',
  styleUrls: ['./input-content.component.css']
})

export class InputContentComponent extends InputClassContent implements OnInit, AfterViewChecked, OnDestroy {
  hide = true;
  esEspecial: boolean;
  constructor(
    private changeDetector: ChangeDetectorRef
  ) {
    super();
    this.esEspecial = false;
  }

  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }

  ngOnInit() {
    setTimeout(() => {
      this.esEspecial =
        this.contenido.type === 'date' ||
        this.contenido.type === 'phone' ||
        this.contenido.type === 'url' ||
        this.contenido.type === 'textarea' ||
        this.contenido.type === 'millar' ||
        this.contenido.type === 'month' ||
        this.contenido.type === 'link' ||
        this.contenido.type === 'hora' ||
        this.contenido.type === 'chips';
    }, 0);

    if (this.contenido.data.type === 'email') {
      this.contenido.options.pattern = '[a-zA-Z0-9]+([.][a-zA-Z0-9]+)*@[a-zA-Z0-9]+([.][a-zA-Z0-9]+)*[.][a-zA-Z0-9]{1,40}';
    }
    this.fillValidators();
  }

  ngOnDestroy() {
    this.activo = false;
  }


}
