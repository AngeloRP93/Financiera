import { Component, OnInit, OnDestroy } from '@angular/core';
import { InputClassContent } from '../input-content.class';

@Component({
  selector: 'app-text-area',
  templateUrl: './text-area.component.html',
  styleUrls: ['./text-area.component.scss']
})
export class TextAreaComponent extends InputClassContent implements OnInit, OnDestroy {

  constructor() {
    super();
  }

  ngOnInit() {
    this.fillValidators();
  }

  ngOnDestroy() {
    this.activo = false;
  }

}
