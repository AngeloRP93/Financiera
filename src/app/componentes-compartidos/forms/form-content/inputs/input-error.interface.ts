export interface InputErrorInterface {
  required: string | 'Deberas ingresar un valor';
  pattern?: string | 'Correo electronico no válido';
  min?: string;
  max?: string;
}
