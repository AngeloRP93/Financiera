import { InputClassContent } from './input-content.class';
import * as _moment from 'moment';
const moment = _moment;

export class InputFecha extends InputClassContent {
  constructor() {
    super();
  }

  protected devolverFecha(value: string): string {
    let fecha;
    if (value) {
      if (_moment.isMoment(value)) {
        fecha = moment(value).format('DD/MM/YYYY');
      } else {
        if (value.includes('/')) {
          fecha = value;
        } else {
          return value;
        }
      }
      const array = fecha.split('/');
      const nuevaFecha = array[2] + '-' + array[1] + '-' + array[0].substring(0, 2);
      return nuevaFecha;
    } else {
      return null;
    }
  }

  protected fillContenido() {
    setTimeout(() => {
      this.contenido.formControl.setValue(this.devolverFecha(this.contenido.data.value), { emitEvent: false });
    }, 10);
  }

  actualizarValor() {
    setTimeout(() => {
      this.contenido.formControl.valueChanges.subscribe(
        (value) => {
          this.contenido.data.value = this.devolverFecha(value);
          if (value !== null) {
            this.emitirValueChanged(this.contenido.data);
          }
        }
      );
    }, 0);
  }
}
