import {
  ElementRef,
  HostListener,
  Directive,
  OnInit,
  OnChanges,
  Input,
  Output,
  EventEmitter,
  Renderer,
  SimpleChanges } from '@angular/core';

@Directive({
    selector: '[appDecimal]'
})
export class DecimalMaskDirective implements OnInit {
    @Input() value: any;
    @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
    posicionPuntoDecimal = -1;
    intervalo: any;
    v_fun;
    constructor(private el: ElementRef, private render: Renderer) { }

    @HostListener('keypress', ['$event']) prevenirNoNumeros(event) {
        const validKeys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'Backspace', '.'];
        if (
            validKeys.indexOf(event.key) !== -1
        ) {
            const valorAnterior = this.el.nativeElement.value + '';
            if (valorAnterior !== '') {
                this.posicionPuntoDecimal = valorAnterior.indexOf('.')
                if (this.posicionPuntoDecimal !== -1) { // Se ha ingresado un punto
                    if (event.key === '.') { // Validación para no dejar ingresar más puntos despues del primer punto ingresado
                        event.preventDefault();
                        event.stopPropagation();
                        return false;
                    } else {
                        if (this.posicionPuntoDecimal + 2 >= valorAnterior.length) { // Validación para no permitir mas de 2 decimales
                            return true;
                        } else {
                            event.preventDefault();
                            event.stopPropagation();
                            return false;
                        }
                    }
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else {
            event.preventDefault();
            event.stopPropagation();
            return false;
        }

    }

    @HostListener('keyup', ['$event']) onInputChange(event) {
        if (
            event.key === '0' ||
            event.key === '1' ||
            event.key === '2' ||
            event.key === '3' ||
            event.key === '4' ||
            event.key === '5' ||
            event.key === '6' ||
            event.key === '7' ||
            event.key === '8' ||
            event.key === '9' ||
            event.key === 'Backspace' ||
            event.key === '.'
        ) {
            if (event.key !== '.') {
                clearInterval(this.intervalo);
                this.intervalo = setInterval(() => {
                    if (this.el.nativeElement.value !== undefined) {
                        if (this.el.nativeElement.value != null) {
                            if (this.el.nativeElement.value !== '') {
                                this.el.nativeElement.value = this.transformar(this.el.nativeElement.value);
                                clearInterval(this.intervalo);
                            }
                        }
                    }
                }, 30);
            }
        }
    }

    parsearString(numero_cadena: string) {
        return numero_cadena.replace(/,/g, '');
    }

    transformar(numero: any) {
        let nuevoNumero;
        nuevoNumero = parseFloat(this.parsearString(numero + ''));
        return nuevoNumero.toLocaleString('en-US');
    }

    ngOnInit() {
        setTimeout(() => {
            if (this.value != null) {
                if (this.value !== '') {
                    this.el.nativeElement.value = this.transformar(this.value);
                }
            }
        }, 100);
    }

}
