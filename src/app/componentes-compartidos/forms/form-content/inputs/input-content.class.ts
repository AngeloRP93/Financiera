
import { Input } from '@angular/core';
import { FormContent } from '../form-content';
import { FieldsetInput } from '../fieldset/fieldset.interface';
export class InputClassContent extends FormContent {
    @Input() contenido: FieldsetInput;
    constructor() {
        super();
    }
}
