import { Component, OnInit, OnDestroy } from '@angular/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { InputFecha } from '../input-fecha';
export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.css'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class InputDateComponent extends InputFecha implements OnInit, OnDestroy {
  // datePattern = '([0-9]{2,2}[/]){2,2}[0-9]{4,4}';
  constructor() {
    super();
    // this.formControl = new FormControl('', [Validators.required, Validators.pattern(this.datePattern)]);
  }

  /*formatoFecha(fecha: string) {
    const indice = fecha.lastIndexOf('-') + 1;
    const ultimo = fecha.substring(indice, indice + 2);
    return fecha.substring(0, indice) + ultimo;
  }*/

  ngOnInit() {
    this.fillValidators();
  }

  ngOnDestroy() {
    this.activo = false;
  }

}
