export interface InputOptions {
    title?: string;
    min?: any;
    max?: any;
    apariencia?: 'legacy' | 'standard' | 'fill' | 'outline';
    pattern?: any;
}
