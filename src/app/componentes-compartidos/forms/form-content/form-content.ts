import { Output, EventEmitter } from '@angular/core';
import { ValidatorFn, FormControl, Validators } from '@angular/forms';
import { FieldsetContent } from './fieldset/fieldset.interface';
import { takeWhile } from 'rxjs/operators';


export class FormContent extends FieldsetContent {
    @Output() changeValue = new EventEmitter<any>();
    validators: ValidatorFn[];
    activo = true;
    constructor() {
        super();
        this.actualizarValor();
    }

    actualizarValor() {
        setTimeout(() => {
            const result = this.contenido.formControl.valueChanges.pipe(takeWhile(() => this.activo)); // Por alguna razon se emite 2 veces
            result.subscribe(
                (value) => {
                    this.contenido.data.value = value;
                    if (value !== null) {
                        this.emitirValueChanged(this.contenido.data);
                    }
                }
            );
        }, 1000);
    }

    emitirValueChanged(data: any) {
        this.changeValue.emit(data);
    }

    protected fillValidators() {
        this.validators = [];
        if (this.contenido.data.isRequired) {
            this.validators.push(Validators.required);
        }

        if (this.contenido.options.min) {
            this.validators.push(Validators.min(this.contenido.options.min));
        }
        if (this.contenido.options.max) {
            this.validators.push(Validators.max(this.contenido.options.max));
        }
        if (this.contenido.options.pattern) {
            this.validators.push(Validators.pattern(this.contenido.options.pattern));
        }


        this.contenido.formControl = new FormControl('', this.validators);
        this.fillContenido();

    }

    protected fillContenido() {
        setTimeout(() => {
            this.contenido.formControl.setValue(this.contenido.data.value, { emitEvent: false });
        }, 1);
    }


    operation() {

    }

    getErrorMessage() {
        return this.contenido.formControl.hasError('required') ? this.contenido.advertencia.required :
            this.contenido.formControl.hasError('pattern') ? this.contenido.advertencia.pattern :
                this.contenido.formControl.hasError('min') ? this.contenido.advertencia.min :
                    this.contenido.formControl.hasError('max') ? this.contenido.advertencia.max :
                        '';
    }
}
