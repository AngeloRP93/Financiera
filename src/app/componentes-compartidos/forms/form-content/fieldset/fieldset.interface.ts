import { IconoInterface } from '../icono/icono.interface';
import { InputInterface } from '../inputs/input.interface';
import { InputOptions } from '../inputs/input-options.interface';
import { TypeSelect } from '../selects/enum-type-select';
import { SelectInterface } from '../selects/select.interface';
import { InputErrorInterface } from '../inputs/input-error.interface';
import { FormControl, Validators } from '@angular/forms';
import { Input } from '@angular/core';
import { InputLabelInterface } from '../inputs/input_label.interface';

export class FielsetElement {
  imagen: IconoInterface;
  clase: string;
  id: string;
  advertencia: InputErrorInterface;
  formControl: FormControl;
  options: InputOptions;

  constructor(
    imagen: IconoInterface = { clase: '' },
    clase: string = '',
    id: string = '',
    advertencia: InputErrorInterface = {
      required: 'Deberias ingresar un valor',
      pattern: 'Correo electronico no válido',
      min: '',
      max: ''
    },
    formControl: FormControl = new FormControl('', [Validators.required]),
    options: InputOptions = {
      apariencia: 'legacy'
    }
  ) {
    this.imagen = imagen;
    this.clase = clase;
    this.id = id;
    this.advertencia = advertencia;
    this.formControl = formControl;
    this.options = options;
  }

  public toString = (): string => {

    return `FielsetElement (imagen: ${this.imagen}, clase: ${this.clase}, )`;
  }
}

export class FieldsetInput extends FielsetElement {
  type: 'text' | 'number' | 'email' | 'date' | 'password' | 'phone' | 'millar' | 'url' | 'textarea' | 'month' | 'link' | 'hora' | 'chips';
  data: InputInterface;
  constructor(
    data: InputInterface = new InputInterface(),
    id: string = '',
    advertencia: InputErrorInterface = {
      required: 'Deberias ingresar un valor',
      pattern: 'Correo electronico no válido',
      min: '',
      max: ''
    },
    options: InputOptions = {
      apariencia: 'legacy'
    },
    imagen: IconoInterface = { clase: '' },
    clase: string = '',
    formControl: FormControl = new FormControl('', [Validators.required]),
  ) {
    super(
      imagen,
      clase,
      id,
      advertencia,
      formControl,
      options
    );
    this.type = data.type;
    this.data = data;
  }

  public toString = (): string => {

    return `FieldsetInput (${this.data})`;
  }
}

export class FieldsetSelect extends FielsetElement {
  type: TypeSelect;
  data: SelectInterface;

  constructor(
    type: TypeSelect = TypeSelect.select,
    data: SelectInterface = new SelectInterface(),
    id: string = '',
    advertencia: InputErrorInterface = {
      required: 'Deberias ingresar un valor',
      pattern: 'Correo electronico no válido',
      min: '',
      max: ''
    },
    options: InputOptions = {
      apariencia: 'legacy'
    },
    imagen: IconoInterface = { clase: '' },
    clase: string = '',
    formControl: FormControl = new FormControl('', [Validators.required])
  ) {
    super(
      imagen,
      clase,
      id,
      advertencia,
      formControl,
      options
    );
    this.type = type;
    this.data = data;
  }

  public toString = (): string => {

    return `FieldsetSelect (data: ${this.data})`;
  }
}

/**
 * Por default es Input
 */
export class FieldsetContent {
  @Input() contenido: any | FieldsetInput | FieldsetSelect;
  @Input() fieldsetId = '';
  constructor(
    esInput: any = true,
    idContenido: string = '',
    value: any = null,
    type: any = 'text',
    label: InputLabelInterface = {
      hasLabel: true,
      texto: ''
    },
    advertencia: InputErrorInterface = {
      required: ''
    },
    // 'text' | 'number' | 'email' | 'date' | 'password' | 'phone' | 'millar' | 'url' | 'textarea' | TypeSelect
    options: InputOptions = {
      apariencia: 'legacy'
    },
    isSeteable: boolean = true,
    isRequired: boolean = true,
    imagen: IconoInterface = { clase: '' },
    clase: string = '',
    optionsSelect: { id: any, nombre: any }[] = [],
    selectedItems: { id: any, nombre: any }[] = [],
    dropdownSettings: any = null,
  ) {
    if (label === null) {
      label = { hasLabel: true, texto: '' };
    }
    if (advertencia === null) {
      advertencia = { required: '' };
    }
    if (options === null) {
      options = { apariencia: 'legacy' };
    }
    if (esInput === true) {
      this.contenido = new FieldsetInput(
        new InputInterface(idContenido, label, value, type, isSeteable, isRequired),
        'input_' + idContenido,
        advertencia,
        options,
        imagen,
        clase
      );
    } else {
      if (esInput === false) {
        this.contenido = new FieldsetSelect(
          type,
          new SelectInterface(
            idContenido,
            label,
            value,
            'text',
            isSeteable,
            isRequired,
            clase,
            optionsSelect,
            selectedItems,
            dropdownSettings
          ),
          'select_' + idContenido,
          advertencia,
          options,
          imagen,
          clase
        );
      } else {
        this.contenido = esInput;
      }

    }
  }

  public toString = (): string => {

    return `FieldsetContent (contenido: ${this.contenido})`;
  }

  instanceOfSelect(object: any): object is SelectInterface {
    return 'options' in object;
  }

  editarOpcionesInput(options: InputOptions, esInput: boolean = true) {
    this.contenido.options = options;
    if (esInput) {
      this.contenido = new FieldsetInput(
        this.contenido.data, this.contenido.id, this.contenido.advertencia,
        options, this.contenido.imagen, this.contenido.clase, this.contenido.formControl);
    } else {
      this.contenido = new FieldsetSelect(
        this.contenido.type, this.contenido.data, this.contenido.id, this.contenido.advertencia,
        options, this.contenido.imagen, this.contenido.clase, this.contenido.formControl);
    }
  }

}

export interface FieldsetInterface {
  inputs: FieldsetContent[];
  hasLegend: boolean;
  legend: string;
  clase: string;
  id: string;
}
