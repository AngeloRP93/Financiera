import { FieldsetModule } from './fieldset.module';

describe('FieldsetModule', () => {
  let fieldsetModule: FieldsetModule;

  beforeEach(() => {
    fieldsetModule = new FieldsetModule();
  });

  it('should create an instance', () => {
    expect(fieldsetModule).toBeTruthy();
  });
});
