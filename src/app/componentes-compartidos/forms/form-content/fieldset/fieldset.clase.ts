import { Input } from '@angular/core';
import { FieldsetInterface, FieldsetContent } from './fieldset.interface';
import { isString } from 'util';
import { ContenedorFilas } from '../../contenedorFilas';
import { _isNumberValue } from '@angular/cdk/coercion';
import { InputOptions } from '../inputs/input-options.interface';

export class Fieldset extends ContenedorFilas {
    @Input() data: FieldsetInterface;
    inputAgregado: boolean;
    constructor(
        inputs: FieldsetContent[] = [],
        id: string = '',
        legend: string = '',
        clase: string = '',
    ) {
        super();
        let hasLegend = false;
        if (legend !== '') {
            hasLegend = true;
        }
        this.data = {
            id: id,
            clase: clase,
            hasLegend: hasLegend,
            legend: legend,
            inputs: inputs
        };
        this.inputAgregado = false;
    }

    public toString = (): string => {

        return `Fieldset (data: ${this.data})`;
    }

    editarInput(indice: number, value: any) {
        this.data.inputs[indice].contenido.data.value = value;
        this.data.inputs[indice].contenido.formControl.setValue(value, { emitEvent: false });
    }

    activarInput(indice: number) {
        this.data.inputs[indice].contenido.data.isSeteable = true;
    }

    activarLadoDerecho(indice: number) {
        if (indice > - 1 && indice < this.data.inputs.length) {
            for (let index = indice + 1; index < this.data.inputs.length; index++) {
                this.data.inputs[index].contenido.data.isSeteable = true;
                this.data.inputs[index].contenido.data.isRequired = true;
            }
        }
    }

    activarLadoIzquierdo(indice: number) {
        if (indice > - 1 && indice < this.data.inputs.length) {
            for (let index = indice - 1; index > 0; index--) {
                this.data.inputs[index].contenido.data.isSeteable = true;
                this.data.inputs[index].contenido.data.isRequired = false;
            }
        }
    }

    desactivarInput(indice: number) {
        this.data.inputs[indice].contenido.data.isSeteable = false;
    }

    desactivarLadoDerecho(indice: number) {
        if (indice > - 1 && indice < this.data.inputs.length) {
            for (let index = indice + 1; index < this.data.inputs.length; index++) {
                this.data.inputs[index].contenido.data.isSeteable = false;
            }
        }
    }

    desactivarLadoIzquierdo(indice: number) {
        if (indice > - 1 && indice < this.data.inputs.length) {
            for (let index = indice - 1; index > 0; index--) {
                this.data.inputs[index].contenido.data.isSeteable = false;
            }
        }
    }

    editarOpcionesInput(indice: number, options: InputOptions) {
        this.data.inputs[indice].editarOpcionesInput(options);
    }

    agregarInput(input: FieldsetContent) {
        this.data.inputs.push(input);
        this.inputAgregado = true;
    }

    agregarInputs(inputs: FieldsetContent[]) {
        this.eliminarSiAgregado();
        for (let index = 0; index < inputs.length; index++) {
            const input = inputs[index];
            this.data.inputs.push(input);
            this.inputAgregado = true;
        }
    }

    agregarInputsSinEliminar(inputs: FieldsetContent[]) {
        for (let index = 0; index < inputs.length; index++) {
            const input = inputs[index];
            this.data.inputs.push(input);
            this.inputAgregado = true;
        }
    }

    eliminarUltimoInput() {
        this.data.inputs.pop();
    }

    eliminarSiAgregado() {
        if (this.inputAgregado) {
            this.eliminarUltimoInput();
            this.inputAgregado = false;
        }
    }

    agregarSoloUnInput(input: FieldsetContent) {
        this.eliminarSiAgregado();
        this.agregarInput(input);
    }


    mostrarBotonEliminar(): boolean {
        if (isString(this.data.id)) {
            if (
                this.data.id.includes('primeros2') ||
                _isNumberValue(this.data.id)
            ) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
