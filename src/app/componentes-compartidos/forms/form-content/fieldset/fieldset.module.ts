import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FieldsetComponent } from './fieldset.component';
import { InputsModule } from '../inputs/inputs.module';
import { ButtonsModule } from '../../../buttons/buttons.module';

@NgModule({
  imports: [
    CommonModule,
    InputsModule,
    ButtonsModule
  ],
  declarations: [
    FieldsetComponent
  ],
  exports: [
    FieldsetComponent
  ]
})

export class FieldsetModule { }
