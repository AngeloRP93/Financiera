import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FieldsetModule } from './fieldset/fieldset.module';
import { InputsModule } from './inputs/inputs.module';
import { SelectsModule } from './selects/selects.module';


@NgModule({
  imports: [
    CommonModule,
    FieldsetModule,
    InputsModule,
    SelectsModule
  ],
  exports: [
    FieldsetModule
  ],
  declarations: []
})
export class FormContentModule { }
