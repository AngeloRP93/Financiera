import { ComponentesCompartidosModule } from './componentes-compartidos.module';

describe('ComponentesCompartidosModule', () => {
  let componentesCompartidosModule: ComponentesCompartidosModule;

  beforeEach(() => {
    componentesCompartidosModule = new ComponentesCompartidosModule();
  });

  it('should create an instance', () => {
    expect(componentesCompartidosModule).toBeTruthy();
  });
});
