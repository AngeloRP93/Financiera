import { ModalInterface } from './modal.interface';
import { Input } from '@angular/core';
import { MyButtonInterface } from '../../buttons/button/button.interface';
import { TypeButton } from '../../../enums/type-button.enum';

export class Modal {
    @Input() info: ModalInterface = {
        titulo: 'Prueba',
        class: 'mymodal',
        id: 'prueba'
    };
    closeButton: MyButtonInterface[] = [
        {
            titulo: 'Cerrar',
            tooltipTitulo: 'Cerrar',
            id: 'prueba',
            imagen: 'clear',
            class: 'close',
            type: TypeButton.Icon,
            toolTipPosition: 'right',
            disabled: false,
            mostrar: function (data) { return true; }
        }
    ];
}
