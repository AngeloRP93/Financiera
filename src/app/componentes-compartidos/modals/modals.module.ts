import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal/modal.component';
import { MatDialogModule } from '@angular/material';
import { ButtonsModule } from '../buttons/buttons.module';
import { DraggableModule } from '../../directives/draggable/draggable.module';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    ButtonsModule,
    DraggableModule
  ],
  declarations: [
    ModalComponent
  ],
  entryComponents: [
    ModalComponent
  ],
  exports: [
    ModalComponent
  ]
})
export class ModalsModule { }
