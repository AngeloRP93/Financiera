import { LayoutPrincipalModule } from './layout-principal.module';

describe('LayoutPrincipalModule', () => {
  let layoutPrincipalModule: LayoutPrincipalModule;

  beforeEach(() => {
    layoutPrincipalModule = new LayoutPrincipalModule();
  });

  it('should create an instance', () => {
    expect(layoutPrincipalModule).toBeTruthy();
  });
});
