import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatExpansionModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './layout/footer/footer.component';
import { ElementosComponent } from './layout/elementos/elementos.component';
import { EscogerElementoService } from './layout/escoger-elemento.service';
import { ToolbarLayoutModule } from './layout/toolbar/toolbar.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    ToolbarLayoutModule
  ],
  declarations: [LayoutComponent, FooterComponent,ElementosComponent],
  exports: [ LayoutComponent],
  providers: [
    EscogerElementoService
  ]
})
export class LayoutPrincipalModule { }
