import { LayoutInterface } from './layout-interface';
import { Input } from '@angular/core';
import { Usuario } from '../../../usuario.interface';

export class Layout {
    @Input() data: LayoutInterface;
    @Input() color = 'primary';
    @Input() actualizar = true;
    @Input() nombre_variable_titulo = 'titulo';
    protected titulo_temp: string;
    usuario: Usuario;
    activo = true;
    loading = true;
    constructor(tituloInicial: string) {
        this.usuario = JSON.parse(localStorage.getItem('usuario'));
        this.titulo_temp = localStorage.getItem('this.titulo_temp');
        if (this.titulo_temp === undefined || this.titulo_temp === null) {
            this.titulo_temp = tituloInicial;
        }
        this.data = {
            barra_navegacion: null,
            toolbar: { buttonIzquierdo: { icono: 'menu' }, titulo: this.titulo_temp, logout: { icono: 'exit_to_app', mostrar: true }, color: 'primary' }
        }
    }
}