import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ElementoNavegacion } from './elemento-navegacion';
import { TipoElementoNavegacion } from './tipo-elemento-navegacion.enum';
import { EscogerElementoService } from '../escoger-elemento.service';

@Component({
  selector: 'app-elementos',
  templateUrl: './elementos.component.html',
  styleUrls: ['./elementos.component.scss']
})
export class ElementosComponent implements OnInit, OnDestroy {
  tipo: TipoElementoNavegacion;
  @Input() elementos: ElementoNavegacion[] = [];

  constructor(
    public escogerService: EscogerElementoService
  ) { }

  ngOnInit() {
  }

  ngOnDestroy() {

  }

}
