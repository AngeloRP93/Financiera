import { TipoElementoNavegacion } from "./tipo-elemento-navegacion.enum";

export interface ElementoNavegacion {
    tipo: TipoElementoNavegacion;
    icono: string;
    titulo: string;
    ruta: string;
    hijos?: ElementoNavegacion[];
}
