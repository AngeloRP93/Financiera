import { ElementoNavegacion } from "./elemento-navegacion";

export interface BarraNavegacion {
    titulo: string;
    elementos: ElementoNavegacion[];
    titulo2?:string;
    subtitulo?: string;
}
