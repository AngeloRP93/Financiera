import { BarraNavegacion } from "./elementos/barra-navegacion";
import { ToolbarInterface } from "./toolbar/toolbar.interface";

export interface LayoutInterface {
    barra_navegacion: BarraNavegacion;
    toolbar: ToolbarInterface
}
