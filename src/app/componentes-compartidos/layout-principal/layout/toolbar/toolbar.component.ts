import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ToolbarInterface } from './toolbar.interface';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, takeWhile } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToolbarService } from './toolbar.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit, OnDestroy {
  @Input() toolbar: ToolbarInterface;
  @Output() mostrarBarraLateral: EventEmitter<any> = new EventEmitter<any>();
  activo = true;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      takeWhile(() => this.activo),
      map(result => result.matches)
    );
  constructor(
    private router: Router,
    private breakpointObserver: BreakpointObserver,
    private toolbarService: ToolbarService
  ) {
    this.toolbarService.tituloActualizado.pipe(
      takeWhile(() => this.activo)
    ).subscribe(
      (titulo) => {
        this.toolbar.titulo = titulo;
      }
    );
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.activo = false;
  }

  emitirClick() {
    this.mostrarBarraLateral.emit();
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/auth']);
  }

}
