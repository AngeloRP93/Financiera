export interface ToolbarInterface {
    buttonIzquierdo: { icono: string };
    titulo: string;
    logout: { icono: string, mostrar: boolean };
    color: 'primary' | 'accent' | 'warn' | '';
}