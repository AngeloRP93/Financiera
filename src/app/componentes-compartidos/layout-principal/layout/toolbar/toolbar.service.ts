import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn:'root'
})
export class ToolbarService {
  @Output() tituloActualizado:EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  actualizarNombre(nuevoTitulo:string) {
    this.tituloActualizado.emit(nuevoTitulo);
  }

}
