import { TestBed, inject } from '@angular/core/testing';

import { EscogerElementoService } from './escoger-elemento.service';

describe('EscogerElementoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EscogerElementoService]
    });
  });

  it('should be created', inject([EscogerElementoService], (service: EscogerElementoService) => {
    expect(service).toBeTruthy();
  }));
});
