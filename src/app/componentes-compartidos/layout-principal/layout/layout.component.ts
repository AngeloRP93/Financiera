import { Component, OnDestroy, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, takeWhile } from 'rxjs/operators';
import { Layout } from './layout';
import { TipoElementoNavegacion } from './elementos/tipo-elemento-navegacion.enum';
import { EscogerElementoService } from './escoger-elemento.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent extends Layout implements OnInit, OnDestroy {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(
    private escogerService: EscogerElementoService,
    private breakpointObserver: BreakpointObserver) {
    super('Prueba');
    this.data.barra_navegacion = null;
  }

  ngOnInit() {
    setTimeout(() => {
      const result = this.escogerService.elementoEscogido.pipe(takeWhile(() => this.activo));
      result.subscribe(
        titulo => {
          this.data.toolbar.titulo = titulo;
          if (this.actualizar) {
            localStorage.setItem(this.nombre_variable_titulo, titulo);
          }
        }
      );
    }, 10);
    if (this.actualizar) {
      if (localStorage.getItem(this.nombre_variable_titulo)) {
        setTimeout(() => {
          this.data.toolbar.titulo = localStorage.getItem(this.nombre_variable_titulo)
        }, 10);
      }
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.activo = false;
  }

}
