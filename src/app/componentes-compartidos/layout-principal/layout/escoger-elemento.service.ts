import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable()
export class EscogerElementoService {
  @Output() elementoEscogido: EventEmitter<string> = new EventEmitter<string>();
  constructor() { }

  escogerElemento(titulo: string) {
    this.elementoEscogido.emit(titulo);
  }
}
