import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './button/button.component';
import { MatButtonModule, MatIconModule, MatCheckboxModule } from '@angular/material';
import { IconButtonComponent } from './icon-button/icon-button.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FormsModule } from '@angular/forms';
import { GroupButtonsComponent } from './group-buttons/group-buttons.component';
import { CheckboxButtonComponent } from './checkbox-button/checkbox-button.component';
import { FabButtonComponent } from './fab-button/fab-button.component';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    FormsModule,
    MatCheckboxModule
  ],
  declarations: [
    FabButtonComponent,
    ButtonComponent, IconButtonComponent, GroupButtonsComponent, CheckboxButtonComponent],
  exports: [
    FabButtonComponent,
    ButtonComponent, IconButtonComponent, GroupButtonsComponent, CheckboxButtonComponent
  ]
})
export class ButtonsModule { }
