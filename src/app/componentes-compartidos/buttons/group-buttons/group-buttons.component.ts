import { Component, OnInit, Input } from '@angular/core';
import { MyButtonInterface } from '../button/button.interface';
import { TypeButton } from '../../../enums/type-button.enum';
import { MyButton } from '../button/button.class';

@Component({
  selector: 'app-group-buttons',
  templateUrl: './group-buttons.component.html',
  styleUrls: ['./group-buttons.component.scss']
})
export class GroupButtonsComponent extends MyButton implements OnInit {
  @Input() buttons: MyButtonInterface[];
  typeButton = TypeButton;
  constructor() {
    super();
  }

  ngOnInit() {
  }

  click(data) {
    this.clickEvent.emit(data);
  }

}
