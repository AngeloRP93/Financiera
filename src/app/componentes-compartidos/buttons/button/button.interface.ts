import { TooltipPosition } from '@angular/material';
import { TypeButton } from '../../../enums/type-button.enum';
export interface MyButtonInterface {
  titulo: string;
  id: string;
  class: string;
  type: TypeButton;
  disabled: boolean;
  imagen?: string;
  tooltipTitulo?: string;
  toolTipPosition?: TooltipPosition;
  mostrar(data: any): boolean;
  estado?(data: any): boolean; // Obligatorio en caso de que Tipo de Boton sea CheckBox
}
