import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoComponent } from './info/info.component';
import { FechasPipeModule } from '../../pipes/fechas/fechas.module';
import { MatIconModule, MatButtonModule, MatDividerModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FechasPipeModule,
    MatIconModule,
    MatButtonModule,
    MatDividerModule
  ],
  declarations: [InfoComponent],
  exports: [InfoComponent]
})
export class InfoModule { }
