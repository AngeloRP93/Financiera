import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {
  @Input() tituloOpcional = '';
  @Input() titulo = 'PRO ATE';
  @Input() id = 'titulo';
  @Input() ultimaActualizacion: any = moment(new Date(2018, 5, 21)).format('DD/MM/YYYY');
  loading = true;
  fecha = new Date();
  @Output() regresar: EventEmitter<any> = new EventEmitter<any>();
  constructor() {
  }

  ngOnInit() {
    this.loading = false
  }

  eventoRegresar() {
    this.regresar.emit(true);
  }

}
