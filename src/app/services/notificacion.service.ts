import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class NotificacionService {

  constructor(
    private toastr: ToastrService
  ) { }

  public mostrarMensajeInfo(titulo, body = '') {
    setTimeout(() => {
      this.toastr.info(body, titulo);
    }, 0);

  }

  public mostrarMensajeSuccess(titulo, body = '') {
    setTimeout(() => {
      this.toastr.success(body, titulo);
    }, 0);
  }

  public mostrarMensajeErrorServidor() {
    setTimeout(() => {
      this.toastr.error('', 'Error de Servidor');
    }, 0);
  }

  public mostrarMensajeError(titulo = 'Error', body = '') {
    setTimeout(() => {
      this.toastr.error(body, titulo);
    }, 0);
  }

  public camposImcompletos() {
    this.toastr.clear();
    this.toastr.error('', 'Faltan llenar campos');
  }
}
