import { Injectable } from '@angular/core';
import * as  conexion_back from './url.json';
import { NotificacionService } from './notificacion.service';
import { WebAddress } from './web-address';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../usuario.interface.js';
// import { SHA256 } from 'crypto-ts';

// HMACSHA512

@Injectable()
export class ApiService {
  webAddress: WebAddress;
  constructor(
    private http: HttpClient,
    private notificationService: NotificacionService,
    private router: Router
  ) {
    this.webAddress = new WebAddress((<any>conexion_back).url);
  }

  devolverFecha(fecha): string {
    const array = fecha.split('/');
    const nuevaFecha = array[2] + '-' + array[1] + '-' + array[0];
    return nuevaFecha;
  }

  devolverCategoria(categoria) {
    switch (categoria) {
      case 'Usuario':
        return 3;
      case 'Evaluador':
        return 2;
      case 'Administrador':
        return 1;
      default:
        return 3;
    }
  }

  public agregarRutasDominio(ruta: any) {
    this.webAddress.setSiguientes(ruta);
  }

  public agregarHeaders(headers: { name: any, value: any }[]) {
    this.webAddress.addHeaders(headers);
  }

  public login(
    request: {
      email: string,
      password: string
    }
  ) {
    this.evaluate(this.http.post(this.webAddress.getUrl() + '/agentes/login', request).toPromise<any>()).then(
      (usuario: Usuario) => {
        localStorage.setItem('usuario', JSON.stringify(usuario))
        this.router.navigate(['/' + usuario.rol_nombre.toLowerCase()]);
      }
    ).catch(
      () => {
        this.notificationService.mostrarMensajeError('Contraseña o Correo Incorrecto')
      }
    )
  }

  public getData(mostrarAlertaSuccess: boolean = true, mostrarAlertaError: boolean = true): Promise<any> {
    const url = this.webAddress.getUrl();
    let headers = this.webAddress.getHeaders();
    const resultado = this.evaluate(this.http.get(url,
      { headers: headers }).toPromise<any>()).then(
        resultados => {
          if (resultados !== undefined) {
            if (mostrarAlertaSuccess === true && resultados.msg !== undefined) {
              this.notificationService.mostrarMensajeInfo(resultados.msg);
            }
            return resultados;
          } else {
            if (mostrarAlertaError) {
              this.notificationService.mostrarMensajeErrorServidor();
            }
            return null;
          }
        }
      ).catch(
        error => {
          if (mostrarAlertaError) {
            this.notificationService.mostrarMensajeErrorServidor();
          }
          return Promise.reject(new Error(''));
        }
      );
    return resultado;
  }

  public postData(
    body: any, mostrarAlertaSuccess: boolean = true, mostrarAlertaError: boolean = true): Promise<any> {
    const url = this.webAddress.getUrl();
    let headers = this.webAddress.getHeaders();
    const resultado = this.evaluate(this.http.post(url, body,
      { headers: headers }).toPromise<any>()).then(
        resultados => {
          if (resultados !== undefined) {
            return resultados;
          } else {
            if (mostrarAlertaError) {
              this.notificationService.mostrarMensajeErrorServidor();
            }
            return null;
          }
        }
      ).catch(
        error => {
          this.notificationService.mostrarMensajeErrorServidor();
          return Promise.reject(new Error(''));
        }
      );
    return resultado;
  }

  public patchData(
    body: any, mostrarAlertaSuccess: boolean = true, mostrarAlertaError: boolean = true): Promise<any> {
    const url = this.webAddress.getUrl();
    let headers = this.webAddress.getHeaders();
    const resultado = this.evaluate(this.http.patch(url, body,
      { headers: headers }).toPromise<any>()).then(
        resultados => {
          if (resultados !== undefined) {
            if (resultados.rpta !== undefined) {
              if (mostrarAlertaSuccess === true && resultados.msg !== undefined) {
                this.notificationService.mostrarMensajeInfo(resultados.msg);
              }
              return resultados.rpta;
            } else {
              return this.validacionMensajes(resultados);
            }
          } else {
            this.notificationService.mostrarMensajeError(resultados.msg);
            return null;
          }
        }
      ).catch(
        error => {
          this.notificationService.mostrarMensajeErrorServidor();
          return Promise.reject(new Error(''));
        }
      );
    return resultado;
  }

  public putData(
    body: any, mostrarAlertaSuccess: boolean = true, mostrarAlertaError: boolean = true): Promise<any> {
    let headers = this.webAddress.getHeaders();
    const resultado = this.evaluate(this.http.put(this.webAddress.getUrl(), body,
      { headers: headers }).toPromise<any>()).then(
        resultados => {
          if (resultados !== undefined) {
            if (mostrarAlertaSuccess === true && resultados.msg !== undefined) {
              this.notificationService.mostrarMensajeInfo(resultados.msg);
            }
            return resultados;
          } else {
            this.notificationService.mostrarMensajeError(resultados.msg);
            return null;
          }
        }
      ).catch(
        error => {
          this.notificationService.mostrarMensajeErrorServidor();
          return Promise.reject(new Error(''));
        }
      );
    return resultado;
  }

  private validacionMensajes(resultados: any) {
    let msg = '';
    if (resultados.msg) {
      msg = resultados.msg;
    }
    if (resultados.success) {
      this.notificationService.mostrarMensajeInfo(msg);
    } else {
      this.notificationService.mostrarMensajeError(msg);
    }
    return null;
  }


  public evaluate(promise): Promise<any> {
    const router = this.router;

    return promise.then((res: any) => {
      if (res.error) {
        throw new Error(res.message);
      }
      return Promise.resolve(res);
    }).catch((error: any) => {
      let message = 'Ocurrió un problema al procesar su solicitud';
      switch (error.status || 500) {
        case 401: // Unauthorized
          setTimeout(function () {
            router.navigate(['/login']);
          }, 400);
          message = 'Necesita iniciar sesión';
          break;
        case 403: // Forbidden
          message = 'Recurso no autorizado';
          break;
        case 404: // Not Found
          message = 'Recurso no encontrado';
          break;
        case 0:   // Unknow Api Server State
        case 500: // Internal Server Error
        case 502: // Bad Gateway
        case 503: // Service Unavailable
        case 504: // Gateway Timeout
          message = 'No se pudo conectar con el servidor';
          break;
      }
      message = error.message || message;
      console.error(`${error.status} - ${error.statusText}: ${message}`);
      return Promise.reject(new Error(message));
    });
  }
}
