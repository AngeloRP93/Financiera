import { OeschleServicesModule } from './oeschle-services.module';

describe('OeschleServicesModule', () => {
  let oeschleServicesModule: OeschleServicesModule;

  beforeEach(() => {
    oeschleServicesModule = new OeschleServicesModule();
  });

  it('should create an instance', () => {
    expect(oeschleServicesModule).toBeTruthy();
  });
});
