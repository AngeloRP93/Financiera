import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { isArray } from 'util';
import { PorcentajeOeschleResultado } from './oeschle.service';

@Injectable()
export class JefesComercialesService {

  constructor(
    private api: ApiService
  ) { }

  obtener_divisiones_plazo(cod_jefe_comercial: number): Promise<any> {
    this.api.webAddress.setSiguientes('CrdLisDivPart')
    return this.api.postData(
      {
        cod_jefe_comercial: cod_jefe_comercial
      }
    ).then(
      (oeschle) => {
        return oeschle;
      }
    ).catch(
      (error) => {
        return [
          {
            Cod_division: 0,
            Division: 'DECOHOGAR'
          },
          {
            Cod_division: 1,
            Division: 'OTRO 1'
          },
          {
            Cod_division: 2,
            Division: 'OTRO 2'
          }
        ];
      }
    );
  }

  obtener_divisiones_part(cod_jefe_comercial: number): Promise<any> {
    this.api.webAddress.setSiguientes('CrdLisDivPart');
    return this.api.postData(
      {
        cod_jefe_comercial: cod_jefe_comercial
      }
    ).then(
      (oeschle) => {
        return oeschle;
      }
    ).catch(
      (error) => {
        return [
          {
            Cod_division: 0,
            Division: 'DECOHOGAR'
          },
          {
            Cod_division: 1,
            Division: 'OTRO 1'
          },
          {
            Cod_division: 2,
            Division: 'OTRO 2'
          }
        ];
      }
    );
  }

  obtener_plazo_jefes_comerciales(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<any> {
    this.api.webAddress.setSiguientes('CrdDepartPlaz')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin
      }
    ).then(
      (oeschle) => {
        if (!isArray(oeschle)) {
          return [];
        } else {
          return oeschle;
        }

      }
    ).catch(
      (error) => {
        return [
          {
            Cod_jefe_Comercial: 0,
            jefe_comercial: 'Ejecutivo',
            real: 15.0,
            meta: 30.0,
            cumplimiento: 50.0
          }
        ];
      }
    );
  }

  obtener_participacion_jefes_comerciales_division_porcentaje(
    cod_jefe_comercial: number,
    cod_division: number
  ): Promise<PorcentajeOeschleResultado> {
    this.api.webAddress.setSiguientes('CrdPorcDepartPart')
    return this.api.postData(
      {
        cod_jefe_comercial: cod_jefe_comercial,
        cod_division: cod_division
      }
    ).then(
      (oeschle) => {
        return oeschle[0];
      }
    ).catch(
      (error) => {
        return [
          {
            real: 10.0,
            meta: 20.0,
            cumplimiento: 50.0
          }
        ];
      }
    );
  }

  obtener_plazo_jefes_comerciales_division_porcentaje(
    cod_jefe_comercial: number,
    cod_division: number
  ): Promise<PorcentajeOeschleResultado> {
    this.api.webAddress.setSiguientes('CrdPorcDepartPlz')

    return this.api.postData(
      {
        cod_jefe_comercial: cod_jefe_comercial,
        cod_division: cod_division
      }
    ).then(
      (oeschle) => {
        return oeschle[0];
      }
    ).catch(
      (error) => {
        return [
          {
            real: 10.0,
            meta: 20.0,
            cumplimiento: 50.0
          }
        ];
      }
    );
  }

  obtener_plazo_jefes_comerciales_division(
    cod_jefe_comercial: number,
    cod_division: number
  ): Promise<any> {
    // CrdLisDepartPart/77718/T10
    this.api.webAddress.setSiguientes('CrdLisDepartPlaz')
    return this.api.postData(
      {
        cod_jefe_comercial: cod_jefe_comercial,
        cod_division: cod_division
      }
    ).then(
      (oeschle) => {
        return oeschle;
      }
    ).catch(
      (error) => {
        return [
          {
            division: 'Algo',
            real: 10.0,
            meta: 20.0,
            cumplimiento: 50.0
          }
        ];
      }
    );
  }

  obtener_participacion_jefes_comerciales_division(
    cod_jefe_comercial: number,
    cod_division: number
  ) {
    this.api.webAddress.setSiguientes('CrdLisDepartPart')
    return this.api.postData(
      {
        cod_jefe_comercial: cod_jefe_comercial,
        cod_division: cod_division
      }
    ).then(
      (oeschle) => {
        return oeschle;
      }
    ).catch(
      (error) => {
        return [
          {
            division: 'Algo',
            real: 10.0,
            meta: 20.0,
            cumplimiento: 50.0
          }
        ];
      }
    );
  }

  obtener_participacion_jefes_comerciales(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<any> {
    this.api.webAddress.setSiguientes('CrdDepartPart')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin
      }
    ).then(
      (oeschle) => {
        if (!isArray(oeschle)) {
          return [];
        } else {
          return oeschle;
        }
      }
    ).catch(
      (error) => {
        return [
          {
            Cod_jefe_Comercial: 0,
            jefe_comercial: 'Ejecutivo',
            real: 10.0,
            meta: 20.0,
            cumplimiento: 50.0
          }
        ];
      }
    );
  }

  obtener_footer_participacion(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<PorcentajeOeschleResultado> {
    this.api.webAddress.setSiguientes('CrdPorcenPart')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin
      }
    ).then(
      (oeschle) => {
        return oeschle[0];
      }
    ).catch(
      (error) => {
        return {
          real: 50.0,
          meta: 100.0,
          cumplimiento: 50
        };
      }
    );
  }

  obtener_footer_plazo(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<PorcentajeOeschleResultado> {
    this.api.webAddress.setSiguientes('CrdPorcenPlaz')

    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin
      }
    ).then(
      (oeschle) => {
        return oeschle[0];
      }
    ).catch(
      (error) => {
        return {
          real: 50.0,
          meta: 100.0,
          cumplimiento: 50
        };
      }
    );
  }
}
