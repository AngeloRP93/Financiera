import { TestBed, inject } from '@angular/core/testing';

import { JefesComercialesService } from './jefes-comerciales.service';

describe('JefesComercialesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JefesComercialesService]
    });
  });

  it('should be created', inject([JefesComercialesService], (service: JefesComercialesService) => {
    expect(service).toBeTruthy();
  }));
});
