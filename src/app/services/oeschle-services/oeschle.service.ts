import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';

export interface PorcentajeOeschleResultado { real: number, meta: number, cumplimiento: number }

@Injectable()
export class OeschleService {

  constructor(
    private api: ApiService
  ) { }

  obtener_porcentaje_participacion(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<PorcentajeOeschleResultado> {
    this.api.webAddress.setSiguientes('CrdPorcenPart')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin
      }
    ).then(
      (oeschle) => {
        return oeschle[0];
      }
    ).catch(
      (error) => {
        return {
          real: 50.0,
          meta: 100.0,
          cumplimiento: 50
        };
      }
    );
  }

  obtener_porcentaje_plazo(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<PorcentajeOeschleResultado> {
    this.api.webAddress.setSiguientes('CrdPorcenPlaz')

    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin
      }
    ).then(
      (oeschle) => {
        return oeschle[0];
      }
    ).catch(
      (error) => {
        return {
          real: 50.0,
          meta: 100.0,
          cumplimiento: 50
        };
      }
    );
  }

}
