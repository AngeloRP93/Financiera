import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { isArray } from 'util';
import { PorcentajeOeschleResultado } from './oeschle.service';

export interface Cajero {
  Nombre_cajero: string;
  real: number;
  meta: number;
  cumplimiento: number;
}
@Injectable()
export class CajerosService {

  constructor(
    private api: ApiService
  ) { }

  obtener_footer_plazo(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<PorcentajeOeschleResultado> {
    this.api.webAddress.setSiguientes('CrdPorCajeroPlaz')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin
      }
    ).then(
      (oeschle) => {
        if (!isArray(oeschle)) {
          return {
            real: 20,
            meta: 40,
            cumplimiento: 50
          };
        } else {
          return oeschle[0];
        }
      }
    ).catch(
      () => {
        return {
          real: 20,
          meta: 40,
          cumplimiento: 50
        };
      }
    )
  }

  obtener_footer_participacion(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<PorcentajeOeschleResultado> {
    this.api.webAddress.setSiguientes('CrdPorCajeroPart')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin
      }
    ).then(
      (oeschle) => {
        if (!isArray(oeschle)) {
          return {
            real: 20,
            meta: 40,
            cumplimiento: 50
          };
        } else {
          return oeschle[0];
        }
      }
    ).catch(
      () => {
        return {
          real: 20,
          meta: 40,
          cumplimiento: 50
        };
      }
    )
  }

  obtener_participacion_cajeros(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<Cajero[]> {
    this.api.webAddress.setSiguientes('CrdListCajeroPart')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin
      }
    ).then(
      (oeschle) => {
        if (!isArray(oeschle)) {
          return [];
        } else {
          return oeschle;
        }
      }
    ).catch(
      (error) => {
        return [
          {
            Nombre_cajero: 'Cajero 1',
            real: 15.0,
            meta: 30.0,
            cumplimiento: 50.0
          },
          {
            Nombre_cajero: 'Cajero 2',
            real: 20.0,
            meta: 100.0,
            cumplimiento: 20.0
          }
        ];
      }
    );
  }

  obtener_plazo_cajeros(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<Cajero[]> {
    this.api.webAddress.setSiguientes('CrdListCajeroPlaz')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin
      }
    ).then(
      (oeschle) => {
        if (!isArray(oeschle)) {
          return [];
        } else {
          return oeschle;
        }

      }
    ).catch(
      (error) => {
        return [
          {
            Nombre_cajero: 'Cajero 1',
            real: 15.0,
            meta: 30.0,
            cumplimiento: 50.0
          },
          {
            Nombre_cajero: 'Cajero 2',
            real: 20.0,
            meta: 100.0,
            cumplimiento: 20.0
          }
        ];
      }
    );
  }
}
