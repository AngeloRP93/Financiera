import { TestBed, inject } from '@angular/core/testing';

import { OeschleService } from './oeschle.service';

describe('OeschleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OeschleService]
    });
  });

  it('should be created', inject([OeschleService], (service: OeschleService) => {
    expect(service).toBeTruthy();
  }));
});
