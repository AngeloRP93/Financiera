import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { PorcentajeOeschleResultado } from './oeschle.service';
import { isArray } from 'util';
export interface Vendedor {
  Vendedor_key: number;
  Nombre_Vendedor: string;
  real: number;
  meta: number;
  cumplimiento: number;
}

@Injectable()
export class VendedoresService {

  constructor(
    private api: ApiService
  ) { }

  obtener_divisiones(
    local_key: string
  ): Promise<any> {
    this.api.webAddress.setSiguientes('CrdListDivVend')
    return this.api.postData(
      {local_key:local_key}
    ).then(
      (oeschle) => {
        return oeschle;
      }
    ).catch(
      (error) => {
        return [
          {
            id: 0,
            nombre: 'DECOHOGAR'
          },
          {
            id: 1,
            nombre: 'OTRO 1'
          },
          {
            id: 2,
            nombre: 'OTRO 2'
          }
        ];
      }
    );
  }

  obtener_participacion_vendedores(
    local_key: any,
    cod_division: any,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<Vendedor[]> {
    this.api.webAddress.setSiguientes('CrdListVenPart')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin,
        cod_division:cod_division
      }
    ).then(
      (oeschle) => {
        return oeschle;
      }
    ).catch(
      (error) => {
        return [
          {
            vendedor: 'Ejecutivo',
            real: 10.0,
            meta: 20.0,
            cumplimiento: 50.0
          }
        ];
      }
    );
  }

  obtener_footer_participacion(
    local_key: any,
    cod_division: any,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<PorcentajeOeschleResultado> {
    this.api.webAddress.setSiguientes('CrdPorVenPart')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin,
        cod_division:cod_division
      }
    ).then(
      (oeschle) => {
        if (!isArray(oeschle)) {
          return {
            real: 20,
            meta: 40,
            cumplimiento: 50
          };
        } else {
          return oeschle[0];
        }
      }
    ).catch(
      () => {
        return {
          real: 20,
          meta: 40,
          cumplimiento: 50
        };
      }
    )
  }

  obtener_footer_plazo(
    local_key: any,
    cod_division: any,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<PorcentajeOeschleResultado> {
    this.api.webAddress.setSiguientes('CrdPorVenPlaz')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin,
        cod_division:cod_division
      }
    ).then(
      (oeschle) => {
        if (!isArray(oeschle)) {
          return {
            real: 20,
            meta: 40,
            cumplimiento: 50
          };
        } else {
          return oeschle[0];
        }
      }
    ).catch(
      () => {
        return {
          real: 20,
          meta: 40,
          cumplimiento: 50
        };
      }
    )
  }

  obtener_plazo_vendedores(
    local_key: any,
    cod_division: any,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<Vendedor[]> {
    this.api.webAddress.setSiguientes('CrdListVenPlaz')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin,
        cod_division:cod_division
      }
    ).then(
      (vendedores) => {
        return vendedores;
      }
    ).catch(
      (error) => {
        return [
          {
            vendedor: 'Ejecutivo',
            real: 15.0,
            meta: 30.0,
            cumplimiento: 50.0
          }
        ];
      }
    );
  }
}
