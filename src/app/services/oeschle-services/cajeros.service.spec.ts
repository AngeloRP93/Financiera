import { TestBed, inject } from '@angular/core/testing';

import { CajerosService } from './cajeros.service';

describe('CajerosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CajerosService]
    });
  });

  it('should be created', inject([CajerosService], (service: CajerosService) => {
    expect(service).toBeTruthy();
  }));
});
