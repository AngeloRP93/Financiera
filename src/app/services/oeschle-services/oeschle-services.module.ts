import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OeschleService } from './oeschle.service';
import { VendedoresService } from './vendedores.service';
import { JefesComercialesService } from './jefes-comerciales.service';
import { CajerosService } from './cajeros.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    OeschleService,
    VendedoresService,
    JefesComercialesService,
    CajerosService
  ]
})
export class OeschleServicesModule { }
