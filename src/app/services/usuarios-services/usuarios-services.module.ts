import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioService } from './usuario.service';
import { ApiService } from '../api.service';
import { RolService } from './rol.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    ApiService,
    UsuarioService,
    RolService
  ]
})
export class UsuariosServicesModule { }
