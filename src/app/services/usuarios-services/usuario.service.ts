import { Injectable, Output, EventEmitter } from '@angular/core';
import { ApiService } from '../api.service';
import { RequestUsuario } from '../../componentes/graficos/administrador/usuarios/registrar_request.interface';
import { Usuario } from '../../usuario.interface';
import { TipoEventoBack } from '../../enums/tipo_evento_back.enum';
import { NotificacionService } from '../notificacion.service';

@Injectable()
export class UsuarioService {
  @Output() cambioEstado = new EventEmitter<{ tipo: TipoEventoBack, data: any }>();
  constructor(
    public api: ApiService,
    private notificacion:NotificacionService
  ) { }

  listar(): Promise<Usuario[]> {
    this.api.agregarRutasDominio('agentes/listar');
    return this.api.getData().then((usuarios) => {
      return usuarios;
    }
    );
  }


  registrar(
    user: RequestUsuario
  ): Promise<Usuario> {
    this.api.agregarRutasDominio('agentes/crear');
    return this.api.postData(user, true).then(
      (usuario) => {
        this.notificacion.mostrarMensajeSuccess('Usuario registrado exitosamente')
        this.cambioEstado.emit({ tipo: TipoEventoBack.Registrar, data: usuario });
        return usuario;
      }
    );
  }

  editar(
    user: Usuario): Promise<any> {
    this.api.agregarRutasDominio('agentes/actualizar');
    const body = {
      usuario_id: user.usuario_id,
      apellido_paterno: user.apellido_paterno,
      apellido_materno: user.apellido_materno,
      nombres: user.nombres
    }
    return this.api.putData(body, true).then(
      (usuario) => {
        this.notificacion.mostrarMensajeSuccess('Actualización exitosa');
        this.cambioEstado.emit({ tipo: TipoEventoBack.Actualizar, data: user });
        return usuario;
      }
    );
  }

  cambiar_clave(usuario_id: number, password: string) {
    const body = { usuario_id: usuario_id, password: password }
    this.api.agregarRutasDominio('agentes/actualizar_clave');
    return this.api.putData(body, true).then(
      ()=> {
        this.notificacion.mostrarMensajeSuccess('Contraseña actualizada exitosamente')
      }
    )
  }

  cambiar_estado(data: { usuario: Usuario, activar: boolean }) {
    this.api.agregarRutasDominio('agentes/actualizar_estado');
    return this.api.putData({
      usuario_id: data.usuario.usuario_id,
      estado: data.activar
    }, true).then(
      () => {
        data.usuario.estado = data.activar;
        if (data.activar) {
          this.notificacion.mostrarMensajeSuccess('Usuario Activado')
        } else {
          this.notificacion.mostrarMensajeSuccess('Usuario Desactivado')
        }
        this.cambioEstado.emit({ tipo: TipoEventoBack.Actualizar, data: data.usuario });
        return null;
      }
    )
  }

  eliminar(
    id: number
  ) {
    this.api.agregarRutasDominio('agentes/eliminar');
    return this.api.putData({ usuario_id: id }, true).then(
      (usuario) => {
        this.cambioEstado.emit({ tipo: TipoEventoBack.Eliminar, data: null });
        return usuario;
      }
    );
  }



  private categoria(category: string): number {
    switch (category) {
      case 'Administrador':
        return 1;
      case 'Evaluador':
        return 2;
      case 'Incubado':
        return 3;
      case 'Pre-Incubado':
        return 4;
      case 'Incubación':
        return 5;
      case 'Co-incubación':
        return 6;
      case 'Acelerado':
        return 7;
      default:
        return 1;
    }
  }
}
