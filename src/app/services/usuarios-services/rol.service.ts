import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';

export interface Rol {
  rol_id:number;
  nombre:string;
  descripcion:string;
}

@Injectable()
export class RolService {

  constructor(
    private api:ApiService
  ) { }

  listar(): Promise<Rol[]>{
    this.api.agregarRutasDominio('roles/listar')
    return this.api.getData()
  }
}
