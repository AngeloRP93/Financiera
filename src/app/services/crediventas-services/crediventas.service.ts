import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';

@Injectable()
export class CrediventasService {

  constructor(
    private api: ApiService
  ) { }

  obtenerCrediventasGrafico(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<any> {
    this.api.webAddress.setSiguientes('crediventas')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin
      }
    ).then(
      (electro) => {
        return electro[0];
      }
    ).catch(
      (error) => {
        return 0.0;
      }
    );
  }

  obtenerElectroPorcentaje(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<any> {
    this.api.webAddress.setSiguientes('electroPorcentaje')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin
      }
    ).then(
      (electro) => {
        return electro[0];
      }
    ).catch(
      (error) => {
        return 0.0;
      }
    );
  }

  obtenerElectroNoPorcentaje(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<any> {
    this.api.webAddress.setSiguientes('electroNoPorcentaje')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin
      }
    ).then(
      (electro) => {
        return electro[0];
      }
    ).catch(
      (error) => {
        return 0.0;
      }
    );
  }

  obtenerElectroData(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<any> {
    this.api.webAddress.setSiguientes('electro')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin
      }
    ).then(
      (electro) => {
        return electro;
      }
    ).catch(
      (error) => {
        return [];
      }
    );
  }

  obtenerNoElectroData(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<any> {
    this.api.webAddress.setSiguientes('electroNo')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin
      }
    ).then(
      (noelectro) => {
        return noelectro;
      }
    ).catch(
      (error) => {
        return [];
      }
    );
  }
}
