import { TestBed, inject } from '@angular/core/testing';

import { CrediventasService } from './crediventas.service';

describe('CrediventasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CrediventasService]
    });
  });

  it('should be created', inject([CrediventasService], (service: CrediventasService) => {
    expect(service).toBeTruthy();
  }));
});
