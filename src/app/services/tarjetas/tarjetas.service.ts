import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';

@Injectable()
export class TarjetasService {

  constructor(
    private api: ApiService
  ) {
    // this.api.webAddress.addUrl('');
  }

  obtenerEntregadas(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<any> {
    this.api.webAddress.setSiguientes('tcEntregadas');
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio:fecha_inicio,
        fecha_fin:fecha_fin
      }
    ).then(
      (entregadas) => {
        return entregadas;
      }
    ).catch(
      (error) => {
        return [];
      }
    );
  }

  obtenerPorcentajeEntregadas(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<any> {
    this.api.webAddress.setSiguientes('tcEntregadasPorcentaje');
    return this.api.postData(
      {
        "local_key": local_key,
        "fecha_inicio":fecha_inicio,
        "fecha_fin":fecha_fin
      }
    ).then(
      (entregadas) => {
        return entregadas[0].rpta;
      }
    ).catch(
      (error) => {
        return 0;
      }
    );
  }

  obtenerActivadas(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<any> {
    this.api.webAddress.setSiguientes('tcActivadas');
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio:fecha_inicio,
        fecha_fin:fecha_fin
      }
    ).then(
      (activadas) => {
        return activadas;
      }
    ).catch(
      (error) => {
        return [];
      }
    );
  }

  obtenerActivadasSupervisor(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<any> {
    this.api.webAddress.setSiguientes('tcActivadasSupervisor')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio:fecha_inicio,
        fecha_fin:fecha_fin
      }
    ).then(
      (activadasSupervisor) => {
        return activadasSupervisor;
      }
    ).catch(
      (error) => {
        return [];
      }
    );
  }

  obtenerPorcentajeActivadas(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<any> {
    this.api.webAddress.setSiguientes('tcActivadasPorcentaje')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio:fecha_inicio,
        fecha_fin:fecha_fin
      }
    ).then(
      (entregadas) => {
        return entregadas[0].rpta;
      }
    ).catch(
      (error) => {
        return 0;
      }
    );
  }

  obtenerIngresadas(
    local_key: string
  ): Promise<any> {
    this.api.webAddress.setSiguientes('tcIngresadas')
    return this.api.postData({
      local_key:local_key
    }).then(
      (ingresadas) => {
        return ingresadas;
      }
    ).catch(
      (error) => {
        return 0;
      }
    );
  }

  obtenerPorcentajeIngresadas(
    local_key: string,
    fecha_inicio: string,
    fecha_fin: string
  ): Promise<any> {
    this.api.webAddress.setSiguientes('tcIngresadasPorcentaje')
    return this.api.postData(
      {
        local_key: local_key,
        fecha_inicio:fecha_inicio,
        fecha_fin:fecha_fin
      }
    ).then(
      (entregadas) => {
        return entregadas[0].rpta;
      }
    ).catch(
      (error) => {
        return 0;
      }
    );
  }
}
