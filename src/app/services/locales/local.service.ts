import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';

@Injectable()
export class LocalService {

  constructor(
    private api: ApiService
  ) {
    
  }

  obtenerLocales(): Promise<any> {
    this.api.webAddress.setSiguientes('tiendas');
    return this.api.getData().then(
      (locales) => {
        return locales;
      }
    ).catch(
      (error) => {
        return [];
      }
    );
  }
}
