import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/compiler/src/core";
import { RolGuard } from './rol.guard';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'auth',
        pathMatch: 'full'
    },
    {
        path: 'auth',
        loadChildren: '../../src/app/componentes/auth/auth.module#AuthModule'
    },
    {
        path: 'admin',
        loadChildren: '../../src/app/componentes/graficos/administrador/administrador.module#AdministradorModule',
        canActivate: [RolGuard]
    },
    {
        path: 'supervisor',
        loadChildren: '../../src/app/componentes/graficos/supervisor/supervisor.module#SupervisorModule',
        canActivate: [RolGuard]
    },
    {
        path: 'ejecutivo',
        loadChildren: '../../src/app/componentes/graficos/ejecutivo/ejecutivo.module#EjecutivoModule',
        canActivate: [RolGuard]
    },
    {
        path: 'gerente',
        loadChildren: '../../src/app/componentes/graficos/gerente/gerente.module#GerenteModule',
        canActivate: [RolGuard]
    },
    {
        path: 'asesor',
        loadChildren: '../../src/app/componentes/graficos/asesor/asesor.module#AsesorModule',
        canActivate: [RolGuard]
    },
    { path: '**', redirectTo: 'auth' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
